import uproot
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array

ROOT.gROOT.SetBatch(ROOT.kTRUE)
ColorPad = [6,7,8,9,1,2,3,4,5,12]

def drawTH1(input_data, xname, yname, title, nbins, xstart, ystart):
    canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
    if isinstance(input_data, dict) :
        hist_dict = {}
        legend = ROOT.TLegend(0.65,0.75,0.98,0.98)
        count = 0
        for histname, value in input_data.items():
            hist_dict[histname] = ROOT.TH1F(histname, title, nbins, xstart, ystart)
            for i in value:
                hist_dict[histname].Fill(i)
            hist_dict[histname].SetLineColor(ColorPad[count])
            if count == 0:
                hist_dict[histname].GetXaxis().SetTitle(xname)
                hist_dict[histname].GetYaxis().SetTitle(yname)
                hist_dict[histname].Draw("LE")
            else:
                hist_dict[histname].Draw("ELSAME")
            legend.AddEntry(hist_dict[histname] , histname,"le")
            count += 1
        legend.Draw()
    else:
        hist = ROOT.TH1F(title, title, nbins, xstart, ystart)
        for i in input_data:
            hist.Fill(i)
        hist.GetXaxis().SetTitle(xname)
        hist.GetYaxis().SetTitle(yname)
        hist.Draw()
    outname = str(title+"_"+xname+"_"+yname).replace("/","-")
    canvas.SaveAs(str(outname+".pdf"))

def drawTH1Average(input_data, weight, xname, yname, title, nbins, xstart, ystart):
    canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
    hist_weight = ROOT.TH1F(str(title+"_weighted"), title, nbins, xstart, ystart)
    hist_one    = ROOT.TH1F(str(title+"_noweight"), title, nbins, xstart, ystart)
    for i in range(len(input_data)):
        hist_one.Fill( input_data[i] )
        hist_weight.Fill(  input_data[i], weight[i] )
    hist_weight.GetXaxis().SetTitle(xname)
    hist_weight.GetYaxis().SetTitle(yname)
    hist_weight.Divide(hist_one)
    hist_weight.Draw()
    outname = str(title+"_"+xname+"_"+yname).replace("/","-")
    canvas.SaveAs(str(outname+".pdf"))

def draw2DScatter(input_data_x, input_data_y, xname, yname, title ):
    x = [-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0 ]
    y = [-1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]

    # Create a TGraph object
    graph = ROOT.TGraph(len(input_data_x), array('f', input_data_x), array('f', input_data_y))

    # Set the title and axis labels
    graph.SetTitle(title)
    graph.GetXaxis().SetTitle(xname)
    graph.GetYaxis().SetTitle(yname)
    graph.SetMarkerStyle(8)

    # Create a canvas to draw the scatter plot
    canvas = ROOT.TCanvas(title, title, 800, 600)

    # Draw the scatter plot
    graph.Draw("AP")

    # Show the canvas
    outname = str(title+"_"+xname+"_"+yname).replace("/","-")
    canvas.SaveAs(str(outname+".pdf"))
    canvas.Draw()


def runOneFile(input_root_file, output_prefix):
    # Open the root file
    root_file = uproot.open(input_root_file)
    
    # Get the keys (top-level objects) in the root file
    root_tree = root_file["Ftree/Events"]
    keys = root_tree.keys()
    
    ## Check whats in TTree
    #for branch_name in keys:
    #    branch_values = root_tree[branch_name].array()
    #    print(f"Branch name: {branch_name}“")
    #    print(f"Branch values: {branch_values}”")
    
    pds = root_tree.array(library="pd")
    ## Plot
    MCounter = root_tree["MCounter"].array()
    XDWC1 = root_tree["XDWC1"].array()
    XDWC2 = root_tree["XDWC2"].array()
    YDWC1 = root_tree["YDWC1"].array()
    YDWC2 = root_tree["YDWC2"].array()
    #drawTH1(MCounter, "MCounter", "Events/bin", "MCounter_noCut", 200, 0, 1000 )
    #drawTH1(XDWC1,       "XDWC1", "Events/bin",    "XDWC1_noCut", 100, -1000, 1000 )
    #drawTH1(XDWC2,       "XDWC2", "Events/bin",    "XDWC2_noCut", 100, -1000, 1000 )
    #drawTH1(YDWC1,       "YDWC1", "Events/bin",    "YDWC1_noCut", 100, -1000, 1000 )
    #drawTH1(YDWC2,       "YDWC2", "Events/bin",    "YDWC2_noCut", 100, -1000, 1000 )
    
    
    XDWC1_afterCut = []
    XDWC2_afterCut = []
    YDWC1_afterCut = []
    YDWC2_afterCut = []
    
    XDWC_afterCut_diff = []
    YDWC_afterCut_diff = []
    
    # X categories decided by dY
    # 1: -2 to -1
    # 2: -1 to -0.5
    # 3: -0.5 to 0
    # 4: 0 to 0.5
    # 5: 0.5 to 1
    # 6: 1 to 2
    
    XDWC_afterCut_diff = []
    YDWC_afterCut_diff = []
    
    XDWC_afterCut_diff_Category1 = []
    XDWC_afterCut_diff_Category2 = []
    XDWC_afterCut_diff_Category3 = []
    XDWC_afterCut_diff_Category4 = []
    XDWC_afterCut_diff_Category5 = []
    XDWC_afterCut_diff_Category6 = []
    
    SiPM_Category1 = []
    SiPM_Category2 = []
    SiPM_Category3 = []
    SiPM_Category4 = []
    SiPM_Category5 = []
    SiPM_Category6 = []
    
    PMT_Category4 = []
    Total_Category4 = []
    
    for event_index, branch_values in pds.iterrows():
        if branch_values["MCounter"] > 300 :
            XDWC1_afterCut.append( branch_values["XDWC1"] )
            XDWC2_afterCut.append( branch_values["XDWC2"] )
            YDWC1_afterCut.append( branch_values["YDWC1"] )
            YDWC2_afterCut.append( branch_values["YDWC2"] )
            
            #print( branch_values["totSiPMSene"] )
            
            XDWC_afterCut_diff.append( branch_values["XDWC1"] - branch_values["XDWC2"] - 3.185872 )
            YDWC_afterCut_diff.append( branch_values["YDWC1"] - branch_values["YDWC2"] - 4.551023 )
            
            print(branch_values["SPMTenergy"],branch_values["CPMTenergy"])
            
            dX = branch_values["XDWC1"] - branch_values["XDWC2"] - 3.185872
            dY = branch_values["YDWC1"] - branch_values["YDWC2"] - 4.551023
            if abs(dX) < 0.5 :
                if abs(dY) < 0.5:
                    SiPM_Category4.append(branch_values["totSiPMSene"]+branch_values["totSiPMCene"])
                    PMT_Category4.append(branch_values["SPMTenergy"]+branch_values["CPMTenergy"])
                    Total_Category4.append( branch_values["totSiPMSene"]+branch_values["totSiPMCene"]+branch_values["SPMTenergy"]+branch_values["CPMTenergy"] )
                    XDWC_afterCut_diff_Category4.append(dX)
                if abs(dY) < 2 and abs(dY) > 1 :
                    SiPM_Category1.append(branch_values["totSiPMSene"]+branch_values["totSiPMCene"])
                    XDWC_afterCut_diff_Category1.append(dX)
                elif abs(dY) < 1 and abs(dY) > 0.5 :
                    SiPM_Category2.append(branch_values["totSiPMSene"]+branch_values["totSiPMCene"])
                    XDWC_afterCut_diff_Category2.append(dX)
                elif abs(dY) < 0.5 :
                    SiPM_Category3.append(branch_values["totSiPMSene"]+branch_values["totSiPMCene"])
                    XDWC_afterCut_diff_Category3.append(dX)
            
            #print( branch_values["XDWC1"],  branch_values["XDWC2"], branch_values["YDWC1"],  branch_values["YDWC2"] )
            #print( branch_values["MCounter"] )
    #drawTH1(XDWC1_afterCut,       "XDWC1", "Events/bin",    "XDWC1_MuonCut", 100, -1000, 1000 )
    #drawTH1(XDWC2_afterCut,       "XDWC2", "Events/bin",    "XDWC2_MuonCut", 100, -1000, 1000 )
    #drawTH1(YDWC1_afterCut,       "YDWC1", "Events/bin",    "YDWC1_MuonCut", 100, -1000, 1000 )
    #drawTH1(YDWC2_afterCut,       "YDWC2", "Events/bin",    "YDWC2_MuonCut", 100, -1000, 1000 )
    
    #drawTH1( {"XDWC1": XDWC1_afterCut , "XDWC2": XDWC2_afterCut }, "XDWC", "Events/bin",    "XDWC-12compare", 50, -20, 20 )
    #drawTH1( {"YDWC1": YDWC1_afterCut , "YDWC2": YDWC2_afterCut }, "YDWC", "Events/bin",    "YDWC-12compare", 50, -20, 20 )
    #drawTH1( {"XDWC1_noCut": XDWC1 , "XDWC1": XDWC1_afterCut }, "XDWC", "Events/bin",    "XDWC-muonCutCompare", 50, -20, 20 )
    #drawTH1( {"YDWC1_noCut": YDWC1 , "YDWC1": YDWC1_afterCut }, "YDWC", "Events/bin",    "YDWC-muonCutCompare", 50, -20, 20 )
    #
    #drawTH1(XDWC_afterCut_diff,       "XDWC", "Events/bin",    "Delta(X1-X2)", 100, -10, 10 )
    #drawTH1(YDWC_afterCut_diff,       "YDWC", "Events/bin",    "Delta(Y1-Y2)", 100, -10, 10 )
    
    print( "dx: " , sum(XDWC_afterCut_diff)/len(XDWC_afterCut_diff) )
    print( "dy: " , sum(YDWC_afterCut_diff)/len(YDWC_afterCut_diff) )
    
    #root_file = uproot.open("../exampleSamples/physics_sps2023_run149.root")
    
    #draw2DScatter(XDWC_afterCut_diff_Category1, SiPM_Category1, "dX", "totSiPM", str( output_prefix +" dY in 2 to 1") )
    #draw2DScatter(XDWC_afterCut_diff_Category2, SiPM_Category2, "dX", "totSiPM", str( output_prefix +" dY in 1 to 0.5") )
    #draw2DScatter(XDWC_afterCut_diff_Category3, SiPM_Category3, "dX", "totSiPM", str( output_prefix +" dY in 0.5 to 0") )
    #
    #drawTH1Average(XDWC_afterCut_diff_Category1, SiPM_Category1, "dX", "averageEnergySiPM", str( output_prefix + " dY in 2 to 1"), 20, -2, 2)
    #drawTH1Average(XDWC_afterCut_diff_Category2, SiPM_Category2, "dX", "averageEnergySiPM", str( output_prefix + " dY in 1 to 0.5"), 20, -2, 2)
    #drawTH1Average(XDWC_afterCut_diff_Category3, SiPM_Category3, "dX", "averageEnergySiPM", str( output_prefix + " dY in 0.5 to 0"), 20, -2, 2)
    #drawTH1Average(XDWC_afterCut_diff_Category4, SiPM_Category4, "dX", "averageEnergySiPM", str( output_prefix + " dY in 2 to 0"), 20, -2, 2)
    #
    drawTH1Average(XDWC_afterCut_diff_Category4, SiPM_Category4, "dX", "averageEnergySiPM", str( output_prefix + " 2 to 0 and 2 to 0"), 5, -0.5, 0.5)
    drawTH1Average(XDWC_afterCut_diff_Category4, PMT_Category4, "dX", "averageEnergyPMT", str( output_prefix + " 2 to 0 and 2 to 0"), 5, -0.5, 0.5)
    drawTH1Average(XDWC_afterCut_diff_Category4, Total_Category4, "dX", "averageEnergySiPM+PMT", str( output_prefix + " 2 to 0 and 2 to 0"), 5, -0.5, 0.5)
    
    print( output_prefix+" Average energe in SiPM: " , sum(SiPM_Category4)/len(SiPM_Category4) )
    print( output_prefix+" Average energe in PMT: "  , sum(PMT_Category4)/len(PMT_Category4) )
    print( output_prefix+" Average energe in Total: ", (sum(PMT_Category4)+sum(SiPM_Category4))/len(PMT_Category4) )


#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run225.root", output_prefix = "2p5")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run227.root", output_prefix = "2p0")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run228.root", output_prefix = "1p0")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run229.root", output_prefix = "0p5")
runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run230.root", output_prefix = "0p0")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run231.root", output_prefix = "m0p5")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run232.root", output_prefix = "m1p0")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run233.root", output_prefix = "m2p0")
#runOneFile(input_root_file = "../exampleSamples/physics_sps2023_run234.root", output_prefix = "m2p5")


## Loop over the keys to find branches
#for key in keys:
#    # Access the key as a TTree
#    tree = root_file[key]
#
#    # Get the list of branch names in the tree
#    branch_names = tree.keys()
#
#    # Loop over the branch names and read their values
#    for branch_name in branch_names:
#        # Read the branch as an array
#        branch_values = tree[branch_name].array()
#
#    # Print the branch name and values
#    print(f"Branch name: {branch_name}“")
#    print(f"Branch values: {branch_values}”")
#    print(“===============================”)


#2.5 Average energe in SiPM:  1.8836760110525155
#2.5 Average energe in PMT:  -2.8307195634978286
#2.5 Average energe in Total:  -0.9470435524453131
#
#2.0 Average energe in SiPM:  2.145996659418907
#2.0 Average energe in PMT:  -2.847196609874204
#2.0 Average energe in Total:  -0.701199950455297
#
#1.0 Average energe in SiPM:  3.101888469170495
#1.0 Average energe in PMT:  -3.5604665825480453
#1.0 Average energe in Total:  -0.4585781133775502
#
#0.5 Average energe in SiPM:  3.59501971810906
#0.5 Average energe in PMT:  -3.9160592774078435
#0.5 Average energe in Total:  -0.32103955929878364
#
#0.0 Average energe in SiPM:  3.3977276665822833
#0.0 Average energe in PMT:  -4.076442796082659
#0.0 Average energe in Total:  -0.6787151295003749
#
#-0.5 Average energe in SiPM:  3.2485874383253166
#-0.5 Average energe in PMT:  -3.998883879227699
#-0.5 Average energe in Total:  -0.7502964409023823
#
#-1.0 Average energe in SiPM:  2.7665051189349796
#-1.0 Average energe in PMT:  -3.6294549026580336
#-1.0 Average energe in Total:  -0.862949783723054
#
#-2.0 Average energe in SiPM:  1.941422691772912
#-2.0 Average energe in PMT:  -2.784131557399873
#-2.0 Average energe in Total:  -0.8427088656269606
#
#-2.5 Average energe in SiPM:  1.7451870641730423
#-2.5 Average energe in PMT:  -2.6799352964142975
#-2.5 Average energe in Total:  -0.9347482322412554
#draw2DScatter([2.5, 2.0, 1.0, 0.5, 0.0, -0.5, -1.0, -2.0, -2.5], [1.88,2.14,3.10,3.59,3.40,3.25,2.77,1.94,1.74], "degree", "totSiPM", "2D")


#2.5 Average energe in SiPM:  1.9170419528457159
#2.5 Average energe in PMT:  -2.8776834507209026
#2.5 Average energe in Total:  -0.9606414978751868
#
#2.0 Average energe in SiPM:  2.0507432189989503
#2.0 Average energe in PMT:  -2.7604541917108403
#2.0 Average energe in Total:  -0.70971097271189
#
#1.0 Average energe in SiPM:  2.8897040345829272
#1.0 Average energe in PMT:  -3.7044610526712463
#1.0 Average energe in Total:  -0.814757018088319
#
#0.5 Average energe in SiPM:  3.6698256883488978
#0.5 Average energe in PMT:  -3.9526782850448012
#0.5 Average energe in Total:  -0.28285259669590335
#
#0.0 Average energe in SiPM:  3.323958767040901
#0.0 Average energe in PMT:  -4.114200582097754
#0.0 Average energe in Total:  -0.7902418150568532
#
#-0.5 Average energe in SiPM:  3.3362600698631635
#-0.5 Average energe in PMT:  -4.037499802039596
#-0.5 Average energe in Total:  -0.7012397321764324
#
#-1.0 Average energe in SiPM:  2.693785934570249
#-1.0 Average energe in PMT:  -3.7364626189711863
#-1.0 Average energe in Total:  -1.0426766844009374
#
#-2.0 Average energe in SiPM:  1.9570231731185477
#-2.0 Average energe in PMT:  -2.791376167949948
#-2.0 Average energe in Total:  -0.8343529948313999
#
#-2.5 Average energe in SiPM:  1.821051532671476
#-2.5 Average energe in PMT:  -2.7543253345425733
#-2.5 Average energe in Total:  -0.9332738018710972
draw2DScatter([2.5, 2.0, 1.0, 0.5, 0.0, -0.5, -1.0, -2.0, -2.5], [1.917,2.051,2.890,3.670,3.324,3.336,2.694,1.957,1.821], "degree", "totSiPM", "2D")

