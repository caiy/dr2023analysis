import uproot
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array
import math
import SiPMDisplay


class PhysicalEvent:
    def __init__(self,
                 ):
        return 0

#Branch name: EventID
#
#Branch name: SPMT1, SPMT2, SPMT3, SPMT4, SPMT5, SPMT6, SPMT7, SPMT8
#Branch name: CPMT1, CPMT2, CPMT3, CPMT4, CPMT5, CPMT6, CPMT7, CPMT8
#Branch name: SPMTenergy
#Branch name: CPMTenergy
#
#Branch name: SiPMPheC[160], SiPMPheS[160]
#Branch name: totSiPMCene
#Branch name: totSiPMSene
#Branch name: NSiPMZero
#
#Branch name: XDWC1
#Branch name: XDWC2
#Branch name: YDWC1
#Branch name: YDWC2
#
#Branch name: PShower
#Branch name: MCounter
#Branch name: C1
#Branch name: C2
#Branch name: C3
