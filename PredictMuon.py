import sys
sys.path.append('./src')
import uproot
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array
import math
import SiPMDisplay
import PMTDisplay
import random
import DWCDisplay
import PlotUtils
import time

ROOT.gROOT.SetBatch(ROOT.kTRUE)
ColorPad = [6,7,8,9,1,2,3,4,5,12]

total_average_fraction=[]
total_xaxis=[]

NtuplePath = "/eos/user/i/ideadr/TB2023_H8/CERNDATA/v1.0/recoNtuple/"
NtuplePath_Simulation = "/afs/cern.ch/work/c/caiy/public/FCC/Simulation/build/"

def correctDWCs( pds ):
    pds["DeltaXDWC"] = (pds['XDWC1'] - pds['XDWC2'])
    pds["DeltaYDWC"] = (pds['YDWC1'] - pds['YDWC2'])
    
    correction_x = pds["DeltaXDWC"].sum()/len(pds.index)
    correction_y = pds["DeltaYDWC"].sum()/len(pds.index)    
    pds["DeltaXDWC"] = (pds['XDWC1'] - pds['XDWC2'] - correction_x )
    pds["DeltaYDWC"] = (pds['YDWC1'] - pds['YDWC2'] - correction_y )
    
    pds["DDistance"] = np.sqrt(pds["DeltaXDWC"]*pds["DeltaXDWC"] + pds["DeltaYDWC"]*pds["DeltaYDWC"])
    
    return correction_x, correction_y



def registerPlotList( pds, title_prefix, outfile_prefix ):
    PlotUtils.draw2DScatterColored(pds["XDWC1"], pds["XDWC2"], pds["DeltaYDWC"], xname="XDWC1", yname="XDWC2", title=title_prefix+" DWCx", outfile=outfile_prefix+"_1test" )
    PlotUtils.draw2DScatterColored(pds["YDWC1"], pds["YDWC2"], pds["DeltaXDWC"], xname="YDWC1", yname="YDWC2", title=title_prefix+" DWCy", outfile=outfile_prefix+"_2test" )
    PlotUtils.drawTH1(pds["MCounter"], xname="MCounter", yname="Event", title=title_prefix+ " MCounter", nbins=100, xstart=0, xend=1000, logY=True, outfile=outfile_prefix+"_MCounter" )
    PlotUtils.drawTH1(pds["PShower"], xname="PShower", yname="Event", title=title_prefix+" PShower", nbins=200, xstart=0, xend=2000, logY=True, outfile=outfile_prefix+"_PShower" )
    PlotUtils.drawTH1(pds["C1"], xname="C1", yname="Event", title=title_prefix+" C1", nbins=100, xstart=0, xend=1000, outfile=outfile_prefix+"_C1" )
    PlotUtils.drawTH1(pds["C2"], xname="C2", yname="Event", title=title_prefix+" C2", nbins=100, xstart=0, xend=1000, outfile=outfile_prefix+"_C2" )
    PlotUtils.draw2DScatter(pds["DeltaXDWC"], pds["DeltaYDWC"], xname="DeltaXDWC", yname="DeltaYDWC", title=title_prefix+" DeltaDWCs", outfile=outfile_prefix+"_DeltaDWCs" )
    PlotUtils.drawTH2(pds["DeltaXDWC"], pds["DeltaYDWC"], xname="DeltaXDWC", yname="DeltaYDWC", title=title_prefix+" DeltaDWCs", 
            nbinsx=100, xstart=-50, xend=50, nbinsy=100, ystart=-50, yend=50, outfile=outfile_prefix+"_DeltaDWCs_TH2")
    PlotUtils.drawTH1(pds["DeltaXDWC"], xname="DeltaXDWC", yname="Event", title=title_prefix+" DeltaXDWC", nbins=100, xstart=-50, xend=50, outfile=outfile_prefix+"_DeltaXDWC" )
    PlotUtils.drawTH1(pds["DeltaYDWC"], xname="DeltaYDWC", yname="Event", title=title_prefix+" DeltaYDWC", nbins=100, xstart=-50, xend=50, outfile=outfile_prefix+"_DeltaYDWC" )
    PlotUtils.draw2DScatter(pds["C1"], pds["C2"], xname="C1", yname="C2", title=title_prefix+" C1 vs C2", outfile=outfile_prefix+"_C1vsC2" )
    PlotUtils.drawTH1(pds["totSiPMSene"], xname="SiPM S Energy", yname="Event", title=title_prefix+" totSiPMSene", nbins=100, xstart=0, xend=25, logY=True, outfile=outfile_prefix+"_totSiPMSene" )
    PlotUtils.drawTH1(pds["totSiPMCene"], xname="SiPM C Energy", yname="Event", title=title_prefix+" totSiPMCene", nbins=100, xstart=0, xend=25, logY=True, outfile=outfile_prefix+"_totSiPMCene" )
    PlotUtils.draw2DScatter(pds["XDWC1"], pds["YDWC1"], xname="XDWC1", yname="YDWC1", title=title_prefix+" DWC1", outfile=outfile_prefix+"_DWC1" )
    PlotUtils.draw2DScatter(pds["XDWC2"], pds["YDWC2"], xname="XDWC2", yname="YDWC2", title=title_prefix+" DWC2", outfile=outfile_prefix+"_DWC2" )
    PlotUtils.drawTH2(pds["XDWC1"], pds["YDWC1"], xname="XDWC1", yname="YDWC1", title=title_prefix+" DWC1", 
            nbinsx=100, xstart=-50, xend=50, nbinsy=100, ystart=-50, yend=50, outfile=outfile_prefix+"_DWC1_TH2")
    PlotUtils.drawTH2(pds["XDWC2"], pds["YDWC2"], xname="XDWC2", yname="YDWC2", title=title_prefix+" DWC2", 
            nbinsx=100, xstart=-50, xend=50, nbinsy=100, ystart=-50, yend=50, outfile=outfile_prefix+"_DWC2_TH2")



def runOneFile(input_root_file, output_prefix):
    runnumber = input_root_file[-11:-5]
    # Open the root file
    root_file = uproot.open(input_root_file)
    
    # Get the keys (top-level objects) in the root file
    root_tree = root_file["Ftree/Events"]
    
    pds = root_tree.array(library="pd")
    
    correctDWCs(pds)

    print(" ***** X ***** ", pds["DeltaXDWC"].sum()/len(pds.index))
    print(" ***** Y ***** ", pds["DeltaYDWC"].sum()/len(pds.index))
    # No cut
    
    print("start: ", len(pds))
    registerPlotList(pds, runnumber, output_prefix+runnumber+"_0" )
    #
    pds=pds[ (pds["MCounter"] > 160) ]
    print("MCounter > 160: ", len(pds))
    registerPlotList(pds, runnumber, output_prefix+runnumber+"_1" )
    #
    #pds=pds[ (pds["PShower"] > 380) ]
    #print("PShower > 380: ", len(pds))
    #registerPlotList(pds, runnumber, output_prefix+runnumber+"_2" )
    #
    pds=pds[ (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) ]
    print("DWC < 2: ", len(pds))
    registerPlotList(pds, runnumber, output_prefix+runnumber+"_3" )
    PlotUtils.drawTH2(pds["DeltaXDWC"], pds["DeltaYDWC"], xname="DeltaXDWC", yname="DeltaYDWC", title=runnumber+" DeltaDWCs", 
            nbinsx=30, xstart=-3, xend=3, nbinsy=30, ystart=-3, yend=3, outfile=output_prefix+runnumber+"_3"+"_DeltaDWCs_TH2")
    
    SiPMEnergy = pds["totSiPMSene"].sum() + pds["totSiPMCene"].sum()
    PMTEnergy = pds["SPMTenergy"].sum() + pds["CPMTenergy"].sum()
    
    SiPMEnergy /= len(pds.index)
    PMTEnergy /= len(pds.index)
    
    #print(SiPMEnergy, PMTEnergy)
    return SiPMEnergy,PMTEnergy


def runSimulation(input_root_file, output_prefix, selectPreShower = False, signal_type = 'ALL' ):
    runnumber = ((input_root_file.split("/")[-1]).split("."))[-2]
    # Open the root file
    root_file = uproot.open(input_root_file)
    fFile = ROOT.TFile(input_root_file, "READ")
    fTree = fFile.Get("DRTB23Simout")
    # Get the keys (top-level objects) in the root file
    print( root_file.keys() )
    root_tree = root_file["DRTB23Simout"]
    print( root_tree.keys() )

    barycentreX = []
    barycentreY = []
    barycentreX_width = []
    barycentreY_width = []
    PrimaryX = []
    PrimaryY = []

    nEntries = fTree.GetEntries()
    for i in range(0, nEntries):
        fTree.GetEntry(i)
        if True:
                        
            tmp_list_S = fTree.VectorSignals
            tmp_list_C = fTree.VectorSignalsCher

            #for i in range(160):
            #    tmp_list_S.append( branch_values["VectorSignals[160]["+str(i)+"]"] )
            #    tmp_list_C.append( branch_values["VectorSignalsCher[160]["+str(i)+"]"] )
            tmp_SiPMS =  SiPMDisplay.SiPMDisplay( {"S":list(tmp_list_S), "C":list(tmp_list_C)}, simulation = True )
            if i < 1 and sum(tmp_list_S) + sum(tmp_list_C) > 0:
                print(output_prefix+runnumber+"_Event"+str(i))
                tmp_SiPMS.draw(title="Entry "+str(i), filename=output_prefix+runnumber+"_Event"+str(i) )
                print(fTree.PrimaryX)
                print(fTree.PrimaryY)
            
            if sum(tmp_list_S) + sum(tmp_list_C) > 0:
                tmp_barycentreX, tmp_barycentreY = tmp_SiPMS.GetBarycentre(signal_type=signal_type)
                barycentreX.append(tmp_barycentreX)
                barycentreY.append(tmp_barycentreY)
                PrimaryX.append(fTree.PrimaryX)
                PrimaryY.append(fTree.PrimaryY)
            
                tmp_barycentreWidthX, tmp_barycentreWidthY = tmp_SiPMS.GetBarycentreRMS(signal_type=signal_type)
                barycentreX_width.append( tmp_barycentreWidthX )
                barycentreY_width.append( tmp_barycentreWidthY )
            #pds.loc[event_index,'barycentreX_S'], pds.loc[event_index,'barycentreY_S'] = tmp_SiPMS.GetBarycentre(signal_type="S")
            #pds.loc[event_index,'barycentreX_C'], pds.loc[event_index,'barycentreY_C'] = tmp_SiPMS.GetBarycentre(signal_type="C")
            #pds.loc[event_index,'barycentreWidthX_S'], pds.loc[event_index,'barycentreWidthY_S'] = tmp_SiPMS.GetBarycentreRMS(signal_type="S")
            #pds.loc[event_index,'barycentreWidthX_C'], pds.loc[event_index,'barycentreWidthY_C'] = tmp_SiPMS.GetBarycentreRMS(signal_type="C")

    #pds['barycentreX_CmS'] = pds['barycentreX_C'] - pds['barycentreX_S']
    #pds['barycentreY_CmS'] = pds['barycentreY_C'] - pds['barycentreY_S']
    
    PlotUtils.draw2DScatter(PrimaryX, barycentreX, xname="Primary X [mm]", yname="Barycenter CaloX [mm]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX" )
    PlotUtils.draw2DScatter(PrimaryY, barycentreY, xname="Primary Y [mm]", yname="Barycenter CaloY [mm]", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsCaloY" )
    #
    #PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_C"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [C] [column number]", title=runnumber+" X Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloX_Cherenkov" )
    #PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_C"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [C] [row number]", title=runnumber+" Y Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloY_Cherenkov" )
    #PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_S"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [S] [column number]", title=runnumber+" X Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloX_Scintillator" )
    #PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_S"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [S] [row number]", title=runnumber+" Y Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloY_Scintillator" )
    #
    PlotUtils.drawTH1(barycentreX_width, xname="barycentre WidthX [mm]", yname="Event", title=runnumber+" WidthX", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX" )
    PlotUtils.drawTH1(barycentreY_width, xname="barycentre WidthY [mm]", yname="Event", title=runnumber+" WidthY", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY" )
    
    #PlotUtils.drawTH1(pds["barycentreWidthX_C"], xname="barycentre WidthX [C]", yname="Event", title=runnumber+" WidthX [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Cherenkov" )
    #PlotUtils.drawTH1(pds["barycentreWidthY_C"], xname="barycentre WidthY [C]", yname="Event", title=runnumber+" WidthY [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Cherenkov" )
    #PlotUtils.drawTH1(pds["barycentreWidthX_S"], xname="barycentre WidthX [S]", yname="Event", title=runnumber+" WidthX [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Scintillator" )
    #PlotUtils.drawTH1(pds["barycentreWidthY_S"], xname="barycentre WidthY [S]", yname="Event", title=runnumber+" WidthY [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Scintillator" )
    #
    #PlotUtils.drawTH1(pds["barycentreX_CmS"], xname="C-S Barycenter CaloX [column number]", yname="Event", title=runnumber+" Barycenter X C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_CmS" )
    #PlotUtils.drawTH1(pds["barycentreY_CmS"], xname="C-S Barycenter CaloY [row number]",    yname="Event", title=runnumber+" Barycenter Y C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_CmS")
    #
    #PlotUtils.drawTH1(pds["barycentreX_C"], xname="Barycenter CaloX [C] [column number]", yname="Event", title=runnumber+" Barycenter X [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Cherenkov")
    #PlotUtils.drawTH1(pds["barycentreY_C"], xname="Barycenter CaloY [C] [row number]",    yname="Event", title=runnumber+" Barycenter Y [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Cherenkov")
    #PlotUtils.drawTH1(pds["barycentreX_S"], xname="Barycenter CaloX [S] [column number]", yname="Event", title=runnumber+" Barycenter X [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Scintillator")
    #PlotUtils.drawTH1(pds["barycentreY_S"], xname="Barycenter CaloY [S] [row number]",    yname="Event", title=runnumber+" Barycenter Y [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Scintillator")
    #
    #PlotUtils.draw2DScatterColored(pds["predictX"], pds["barycentreX"], pds["DeltaXDWC"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX_colored" )
    #PlotUtils.draw2DScatterColored(pds["predictX"], pds["PShower"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X_colored" )
    #
    #PlotUtils.draw2DScatter(pds["predictX"], pds["PShower"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X" )
    #PlotUtils.draw2DScatter(pds["predictY"], pds["PShower"], xname="Predict CaloY [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsPS_Y" )
    #
    #PlotUtils.draw2DScatter(pds["XDWC1"], pds["PShower"], xname="DWC1 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC1vsPS_X" )
    #PlotUtils.draw2DScatter(pds["YDWC1"], pds["PShower"], xname="DWC1 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC1vsPS_Y" )
    #
    #PlotUtils.draw2DScatter(pds["XDWC2"], pds["PShower"], xname="DWC2 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC2vsPS_X" )
    #PlotUtils.draw2DScatter(pds["YDWC2"], pds["PShower"], xname="DWC2 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC2vsPS_Y" )
    #
    #PlotUtils.drawTH2(pds["predictX"], pds["predictY"], xname="Predict CaloX [mm]", yname="Predict CaloY [mm]", title=runnumber+" DWC prediction", 
    #        nbinsx=40, xstart=-20, xend=20, nbinsy=40, ystart=-20, yend=20, outfile=output_prefix+runnumber+"_DWCXvsDWCY")
    #PlotUtils.drawTH2(pds["barycentreX"], pds["barycentreY"], xname="Barycenter CaloX [column number]", yname="Barycenter CaloY [row number]", title=runnumber+" Barycenter", 
    #        nbinsx=20, xstart=0, xend=20, nbinsy=20, ystart=0, yend=20, outfile=output_prefix+runnumber+"_CaloXvsCaloY")
    #
    #PlotUtils.drawTH1(pds["barycentreWidthX"], xname="barycentre WidthX", yname="Event", title=runnumber+" WidthX", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX" )
    #PlotUtils.drawTH1(pds["barycentreWidthY"], xname="barycentre WidthY", yname="Event", title=runnumber+" WidthY", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY" )
    
    PlotUtils.drawTH1(barycentreX, xname="Barycenter CaloX [mm]", yname="Event", title=runnumber+" Barycenter X", nbins=32, xstart=0, xend=16, logY=True, outfile=output_prefix+runnumber+"_barycentreX")
    PlotUtils.drawTH1(barycentreY, xname="Barycenter CaloY [mm]", yname="Event", title=runnumber+" Barycenter Y", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY")
    #PlotUtils.drawTH1(pds["predictX"], xname="Predict CaloX [mm]", yname="Event", title=runnumber+" CaloX", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_PredictCaloX" )
    #PlotUtils.drawTH1(pds["predictY"], xname="Predict CaloY [mm]", yname="Event", title=runnumber+" CaloY", nbins=60, xstart=-30, xend=30, logY=True, outfile=output_prefix+runnumber+"_PredictCaloY" )
    return barycentreX, barycentreY




def runSiPM(input_root_file, output_prefix, selectPreShower = False, signal_type = 'ALL' ):
    runnumber = input_root_file[-11:-5]
    # Open the root file
    root_file = uproot.open(input_root_file)
    # Get the keys (top-level objects) in the root file
    root_tree = root_file["Ftree/Events"]

    pds = root_tree.array(library="pd")
    
    correct_x, correct_y = correctDWCs(pds)
    
    pds=pds[ (pds["MCounter"] > 160) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] * pds["totSiPMCene"] > 0 ) ] # & (pds["PShower"] > 380)
    #pds=pds[ (pds["C1"] > 150) & (pds["C2"] > 110) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] + pds["totSiPMCene"] > 0 ) ] # & (pds["PShower"] > 380)
    #registerPlotList(pds, runnumber, output_prefix+runnumber )
    average_fraction = []
    draw10_Active_Board = 0
    print(1)
    st = time.time()
    for event_index, branch_values in pds.iterrows():
        #if branch_values["MCounter"] > 200 and branch_values["totSiPMSene"] > 0.1 :
        if True:
            
            tmp_DWCs = DWCDisplay.DWCDisplay( DWC1_x = branch_values["XDWC1"] , DWC1_y = branch_values["YDWC1"], DWC2_x = branch_values["XDWC2"] , DWC2_y = branch_values["YDWC2"], correction_x = correct_x, correction_y = correct_y )
            pds.loc[event_index,'predictX'], pds.loc[event_index,'predictY'] = tmp_DWCs.GetPredictCenter()
            
            tmp_list_S = []
            tmp_list_C = []
            
            for i in range(160):
                tmp_list_S.append( branch_values["SiPMPheS[160]["+str(i)+"]"] )
                tmp_list_C.append( branch_values["SiPMPheC[160]["+str(i)+"]"] )
            tmp_SiPMS =  SiPMDisplay.SiPMDisplay( {"S":tmp_list_S, "C":tmp_list_C} )
            #pds.loc[event_index,'NumberOfActiveBoard'] = tmp_SiPMS.NumberOfActiveBoard()
            
            pds.loc[event_index,'barycentreX'], pds.loc[event_index,'barycentreY'] = tmp_SiPMS.GetBarycentre(signal_type=signal_type)
            
            pds.loc[event_index,'barycentreWidthX'], pds.loc[event_index,'barycentreWidthY'] = tmp_SiPMS.GetBarycentreRMS(signal_type=signal_type)
            
            pds.loc[event_index,'barycentreX_S'], pds.loc[event_index,'barycentreY_S'] = tmp_SiPMS.GetBarycentre(signal_type="S")
            pds.loc[event_index,'barycentreX_C'], pds.loc[event_index,'barycentreY_C'] = tmp_SiPMS.GetBarycentre(signal_type="C")
            pds.loc[event_index,'barycentreWidthX_S'], pds.loc[event_index,'barycentreWidthY_S'] = tmp_SiPMS.GetBarycentreRMS(signal_type="S")
            pds.loc[event_index,'barycentreWidthX_C'], pds.loc[event_index,'barycentreWidthY_C'] = tmp_SiPMS.GetBarycentreRMS(signal_type="C")
            
            if draw10_Active_Board < 1 :
                print(2)
                tmp_SiPMS.draw(title="Display test "+runnumber+" Event"+str(branch_values["EventID"]), filename=output_prefix+runnumber+"_Event"+str(branch_values["EventID"]) )
                print(3)
            if draw10_Active_Board%1000 == 0:
                et = time.time()
                elapsed_time = et - st
                st = et
                print( 'Execution time ' , draw10_Active_Board, " are done with ", elapsed_time, ' seconds' )
            draw10_Active_Board += 1
            #list_energy_density_x = tmp_SiPMS.GetEnergyDensityX(8,signal_type=signal_type)
            
            #for i in range(-8,9):
            #    pds.loc[event_index,'EnergyFraction'+str(i)] = list_energy_density_x[i+8]
            
            #if tmp_SiPMS.NumberOfActiveBoard() > 0:
            #    if tmp_SiPMS.NumberOfActiveBoard() == 1 and draw10_Active_Board < 5 :
            #        tmp_SiPMS.draw(title="ActiveBoard1 "+runnumber+" Event"+str(branch_values["EventID"]), filename=output_prefix+runnumber+"_Event"+str(branch_values["EventID"]) )
            #        draw10_Active_Board += 1
            #        print(tmp_SiPMS.NumberOfActiveBoard())
            #        print( tmp_SiPMS.PeakPosition() )
            #        print( tmp_SiPMS.PeakPosition(2) )
            #        most_active_board = []
            #        for i in tmp_SiPMS.getActiveBoard():
            #            if ( sum(i[1]) > sum(i[3]) ):
            #                most_active_board = i[1]
            #            else :
            #                most_active_board = i[3]
            #        draw2DScatter(most_active_board, np.arange(1,17,1), xname="signal strength", yname="position", 
            #                     size = 0.8, title=output_prefix+"MostActiveLine "+runnumber+" Event"+str(branch_values["EventID"]))
            #    if tmp_SiPMS.NumberOfActiveBoard() > 4:
            #        print(tmp_SiPMS.NumberOfActiveBoard())
            #        tmp_SiPMS.draw(title="ActiveBoard"+str(tmp_SiPMS.NumberOfActiveBoard())+" "+runnumber+" Event"+str(branch_values["EventID"]), filename=output_prefix+runnumber+"_Event"+str(branch_values["EventID"]) )
            #        print( tmp_SiPMS.PeakPosition() )
            #        print( tmp_SiPMS.PeakPosition(2) )
            #continue
    #drawTH1(pds['NumberOfActiveBoard'], xname="Number of Active Board", yname="Event", title=output_prefix+runnumber, nbins=5, xstart=0, xend=5, logY=True)
    
    #xaxis = []
    #for i in range(-8,9):
    #    average_fraction.append(pds['EnergyFraction'+str(i)].mean() )
    #    xaxis.append(i)
    print(4)
    pds['barycentreX_CmS'] = pds['barycentreX_C'] - pds['barycentreX_S']
    pds['barycentreY_CmS'] = pds['barycentreY_C'] - pds['barycentreY_S']

    #PlotUtils.draw2DScatter(xaxis, average_fraction, xname="Column number", yname="Energy Fraction", title=runnumber+" Fraction on X", outfile=output_prefix+runnumber+"_FractionOnX", size=0.8 )
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [mm]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [mm]", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsCaloY" )
    
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_C"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [C] [mm]", title=runnumber+" X Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloX_Cherenkov" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_C"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [C] [mm]", title=runnumber+" Y Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloY_Cherenkov" )
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_S"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [S] [mm]", title=runnumber+" X Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloX_Scintillator" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_S"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [S] [mm]", title=runnumber+" Y Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloY_Scintillator" )
    
    PlotUtils.drawTH1(pds["barycentreWidthX_C"], xname="barycentre WidthX [C]", yname="Event", title=runnumber+" WidthX [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Cherenkov" )
    PlotUtils.drawTH1(pds["barycentreWidthY_C"], xname="barycentre WidthY [C]", yname="Event", title=runnumber+" WidthY [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Cherenkov" )
    PlotUtils.drawTH1(pds["barycentreWidthX_S"], xname="barycentre WidthX [S]", yname="Event", title=runnumber+" WidthX [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Scintillator" )
    PlotUtils.drawTH1(pds["barycentreWidthY_S"], xname="barycentre WidthY [S]", yname="Event", title=runnumber+" WidthY [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Scintillator" )
    
    PlotUtils.drawTH1(pds["barycentreX_CmS"], xname="C-S Barycenter CaloX [mm]", yname="Event", title=runnumber+" Barycenter X C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_CmS" )
    PlotUtils.drawTH1(pds["barycentreY_CmS"], xname="C-S Barycenter CaloY [mm]",    yname="Event", title=runnumber+" Barycenter Y C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_CmS")
    
    PlotUtils.drawTH1(pds["barycentreX_C"], xname="Barycenter CaloX [C] [mm]", yname="Event", title=runnumber+" Barycenter X [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Cherenkov")
    PlotUtils.drawTH1(pds["barycentreY_C"], xname="Barycenter CaloY [C] [mm]",    yname="Event", title=runnumber+" Barycenter Y [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Cherenkov")
    PlotUtils.drawTH1(pds["barycentreX_S"], xname="Barycenter CaloX [S] [mm]", yname="Event", title=runnumber+" Barycenter X [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Scintillator")
    PlotUtils.drawTH1(pds["barycentreY_S"], xname="Barycenter CaloY [S] [mm]",    yname="Event", title=runnumber+" Barycenter Y [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Scintillator")
    
    #PlotUtils.draw2DScatterColored(pds["predictX"], pds["barycentreX"], pds["DeltaXDWC"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX_colored" )
    #PlotUtils.draw2DScatterColored(pds["predictX"], pds["PShower"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X_colored" )
    
    PlotUtils.draw2DScatter(pds["predictX"], pds["PShower"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["PShower"], xname="Predict CaloY [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC1"], pds["PShower"], xname="DWC1 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC1vsPS_X" )
    PlotUtils.draw2DScatter(pds["YDWC1"], pds["PShower"], xname="DWC1 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC1vsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC2"], pds["PShower"], xname="DWC2 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC2vsPS_X" )
    PlotUtils.draw2DScatter(pds["YDWC2"], pds["PShower"], xname="DWC2 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC2vsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC1"], pds["XDWC2"], xname="DWC1 X [mm]", yname="DWC2 X [mm]", title=runnumber+" DWCs X", outfile=output_prefix+runnumber+"_DWC1XvsDWC2X" )
    PlotUtils.draw2DScatter(pds["YDWC1"], pds["YDWC2"], xname="DWC1 Y [mm]", yname="DWC2 Y [mm]", title=runnumber+" DWCs Y", outfile=output_prefix+runnumber+"_DWC1YvsDWC2Y" )
    
    PlotUtils.drawTH2(pds["predictX"], pds["predictY"], xname="Predict CaloX [mm]", yname="Predict CaloY [mm]", title=runnumber+" DWC prediction", 
            nbinsx=40, xstart=-20, xend=20, nbinsy=40, ystart=-20, yend=20, outfile=output_prefix+runnumber+"_DWCXvsDWCY")
    PlotUtils.drawTH2(pds["barycentreX"], pds["barycentreY"], xname="Barycenter CaloX [mm]", yname="Barycenter CaloY [mm]", title=runnumber+" Barycenter", 
            nbinsx=20, xstart=0, xend=20, nbinsy=20, ystart=0, yend=20, outfile=output_prefix+runnumber+"_CaloXvsCaloY")
    
    PlotUtils.drawTH1(pds["barycentreWidthX"], xname="barycentre WidthX", yname="Event", title=runnumber+" WidthX", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX" )
    PlotUtils.drawTH1(pds["barycentreWidthY"], xname="barycentre WidthY", yname="Event", title=runnumber+" WidthY", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY" )
    
    PlotUtils.drawTH1(pds["barycentreX"], xname="Barycenter CaloX [mm]", yname="Event", title=runnumber+" Barycenter X", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX")
    PlotUtils.drawTH1(pds["barycentreY"], xname="Barycenter CaloY [mm]",    yname="Event", title=runnumber+" Barycenter Y", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY")
    PlotUtils.drawTH1(pds["predictX"], xname="Predict CaloX [mm]", yname="Event", title=runnumber+" CaloX", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_PredictCaloX" )
    PlotUtils.drawTH1(pds["predictY"], xname="Predict CaloY [mm]", yname="Event", title=runnumber+" CaloY", nbins=60, xstart=-30, xend=30, logY=True, outfile=output_prefix+runnumber+"_PredictCaloY" )
    #pds=pds[ (pds["NumberOfActiveBoard"] > 4) ]
    #registerPlotList(pds, runnumber, output_prefix+runnumber+"_5board" )
    return pds['barycentreWidthX'].mean(), pds['barycentreWidthY'].mean(), average_fraction
    if selectPreShower :
        pds=pds[ (pds["predictY"] > 8) & (abs(pds["predictY"]) < 14) & (abs(pds["predictX"]) > 4) & ( pds["predictX"] < 8) ]
        registerPlotList(pds, runnumber, output_prefix+runnumber+"_CenterCut" )



def runSiPM_LessThan380(input_root_file, output_prefix, selectPreShower = False, signal_type = 'ALL' ):
    runnumber = input_root_file[-11:-5]
    # Open the root file
    root_file = uproot.open(input_root_file)
    # Get the keys (top-level objects) in the root file
    root_tree = root_file["Ftree/Events"]

    pds = root_tree.array(library="pd")
    
    correct_x, correct_y = correctDWCs(pds)
    
    pds=pds[ (pds["MCounter"] > 160) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] * pds["totSiPMCene"] > 0 ) ] 
    #pds=pds[ pds["PShower"] < 380 ]
    #pds=pds[ pds["XDWC2"] < -5 ]
    #registerPlotList(pds, runnumber, output_prefix+runnumber )
    average_fraction = []
    draw10_Active_Board = 0
    for event_index, branch_values in pds.iterrows():
        if True:
            
            tmp_DWCs = DWCDisplay.DWCDisplay( DWC1_x = branch_values["XDWC1"] , DWC1_y = branch_values["YDWC1"], DWC2_x = branch_values["XDWC2"] , DWC2_y = branch_values["YDWC2"], correction_x = correct_x, correction_y = correct_y )
            pds.loc[event_index,'predictX'], pds.loc[event_index,'predictY'] = tmp_DWCs.GetPredictCenter()
            
            tmp_list_S = []
            tmp_list_C = []
            
            for i in range(160):
                tmp_list_S.append( branch_values["SiPMPheS[160]["+str(i)+"]"] )
                tmp_list_C.append( branch_values["SiPMPheC[160]["+str(i)+"]"] )
            tmp_SiPMS =  SiPMDisplay.SiPMDisplay( {"S":tmp_list_S, "C":tmp_list_C} )
            
            pds.loc[event_index,'barycentreX'], pds.loc[event_index,'barycentreY'] = tmp_SiPMS.GetBarycentre(signal_type=signal_type)
            
            pds.loc[event_index,'barycentreWidthX'], pds.loc[event_index,'barycentreWidthY'] = tmp_SiPMS.GetBarycentreRMS(signal_type=signal_type)
            
            pds.loc[event_index,'barycentreX_S'], pds.loc[event_index,'barycentreY_S'] = tmp_SiPMS.GetBarycentre(signal_type="S")
            pds.loc[event_index,'barycentreX_C'], pds.loc[event_index,'barycentreY_C'] = tmp_SiPMS.GetBarycentre(signal_type="C")
            pds.loc[event_index,'barycentreWidthX_S'], pds.loc[event_index,'barycentreWidthY_S'] = tmp_SiPMS.GetBarycentreRMS(signal_type="S")
            pds.loc[event_index,'barycentreWidthX_C'], pds.loc[event_index,'barycentreWidthY_C'] = tmp_SiPMS.GetBarycentreRMS(signal_type="C")
    
    pds['barycentreX_CmS'] = pds['barycentreX_C'] - pds['barycentreX_S']
    pds['barycentreY_CmS'] = pds['barycentreY_C'] - pds['barycentreY_S']
    pds=pds[ ( pds["barycentreX_CmS"] < 1 ) & ( pds["barycentreX_CmS"] > -1 ) ]
    pds=pds[ ( pds["barycentreWidthX_S"] < 3 ) & ( pds["barycentreWidthX_C"] < 4 ) ]
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [row number]", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsCaloY" )
    
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_C"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [C] [column number]", title=runnumber+" X Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloX_Cherenkov" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_C"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [C] [row number]", title=runnumber+" Y Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloY_Cherenkov" )
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_S"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [S] [column number]", title=runnumber+" X Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloX_Scintillator" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_S"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [S] [row number]", title=runnumber+" Y Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloY_Scintillator" )
    
    PlotUtils.drawTH1(pds["barycentreWidthX_C"], xname="barycentre WidthX [C]", yname="Event", title=runnumber+" WidthX [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Cherenkov" )
    PlotUtils.drawTH1(pds["barycentreWidthY_C"], xname="barycentre WidthY [C]", yname="Event", title=runnumber+" WidthY [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Cherenkov" )
    PlotUtils.drawTH1(pds["barycentreWidthX_S"], xname="barycentre WidthX [S]", yname="Event", title=runnumber+" WidthX [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Scintillator" )
    PlotUtils.drawTH1(pds["barycentreWidthY_S"], xname="barycentre WidthY [S]", yname="Event", title=runnumber+" WidthY [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Scintillator" )
    
    PlotUtils.drawTH1(pds["barycentreX_CmS"], xname="C-S Barycenter CaloX [column number]", yname="Event", title=runnumber+" Barycenter X C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_CmS" )
    PlotUtils.drawTH1(pds["barycentreY_CmS"], xname="C-S Barycenter CaloY [row number]",    yname="Event", title=runnumber+" Barycenter Y C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_CmS")
    
    PlotUtils.drawTH1(pds["barycentreX_C"], xname="Barycenter CaloX [C] [column number]", yname="Event", title=runnumber+" Barycenter X [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Cherenkov")
    PlotUtils.drawTH1(pds["barycentreY_C"], xname="Barycenter CaloY [C] [row number]",    yname="Event", title=runnumber+" Barycenter Y [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Cherenkov")
    PlotUtils.drawTH1(pds["barycentreX_S"], xname="Barycenter CaloX [S] [column number]", yname="Event", title=runnumber+" Barycenter X [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Scintillator")
    PlotUtils.drawTH1(pds["barycentreY_S"], xname="Barycenter CaloY [S] [row number]",    yname="Event", title=runnumber+" Barycenter Y [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Scintillator")
    
    PlotUtils.draw2DScatterColored(pds["predictX"], pds["barycentreX"], pds["DeltaXDWC"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX_colored" )
    PlotUtils.draw2DScatterColored(pds["predictX"], pds["PShower"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X_colored" )
    
    PlotUtils.draw2DScatter(pds["predictX"], pds["PShower"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["PShower"], xname="Predict CaloY [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC1"], pds["PShower"], xname="DWC1 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC1vsPS_X" )
    PlotUtils.draw2DScatter(pds["YDWC1"], pds["PShower"], xname="DWC1 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC1vsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC2"], pds["PShower"], xname="DWC2 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC2vsPS_X" )
    PlotUtils.draw2DScatter(pds["YDWC2"], pds["PShower"], xname="DWC2 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC2vsPS_Y" )
    
    PlotUtils.drawTH2(pds["predictX"], pds["predictY"], xname="Predict CaloX [mm]", yname="Predict CaloY [mm]", title=runnumber+" DWC prediction", 
            nbinsx=40, xstart=-20, xend=20, nbinsy=40, ystart=-20, yend=20, outfile=output_prefix+runnumber+"_DWCXvsDWCY")
    PlotUtils.drawTH2(pds["barycentreX"], pds["barycentreY"], xname="Barycenter CaloX [column number]", yname="Barycenter CaloY [row number]", title=runnumber+" Barycenter", 
            nbinsx=20, xstart=0, xend=20, nbinsy=20, ystart=0, yend=20, outfile=output_prefix+runnumber+"_CaloXvsCaloY")
    
    PlotUtils.drawTH1(pds["barycentreWidthX"], xname="barycentre WidthX", yname="Event", title=runnumber+" WidthX", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX" )
    PlotUtils.drawTH1(pds["barycentreWidthY"], xname="barycentre WidthY", yname="Event", title=runnumber+" WidthY", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY" )
    
    PlotUtils.drawTH1(pds["barycentreX"], xname="Barycenter CaloX [column number]", yname="Event", title=runnumber+" Barycenter X", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX")
    PlotUtils.drawTH1(pds["barycentreY"], xname="Barycenter CaloY [row number]",    yname="Event", title=runnumber+" Barycenter Y", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY")
    PlotUtils.drawTH1(pds["predictX"], xname="Predict CaloX [mm]", yname="Event", title=runnumber+" CaloX", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_PredictCaloX" )
    PlotUtils.drawTH1(pds["predictY"], xname="Predict CaloY [mm]", yname="Event", title=runnumber+" CaloY", nbins=60, xstart=-30, xend=30, logY=True, outfile=output_prefix+runnumber+"_PredictCaloY" )



def runSiPM_MoreThan380(input_root_file, output_prefix, selectPreShower = False, signal_type = 'ALL' ):
    runnumber = input_root_file[-11:-5]
    # Open the root file
    root_file = uproot.open(input_root_file)
    # Get the keys (top-level objects) in the root file
    root_tree = root_file["Ftree/Events"]

    pds = root_tree.array(library="pd")
    
    correct_x, correct_y = correctDWCs(pds)
    
    pds=pds[ (pds["MCounter"] > 160) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] + pds["totSiPMCene"] > 0 ) ] 
    #pds=pds[ pds["PShower"] > 380 ]
    #pds=pds[ pds["XDWC2"] > -5 ]
    #registerPlotList(pds, runnumber, output_prefix+runnumber )
    average_fraction = []
    draw10_Active_Board = 0
    for event_index, branch_values in pds.iterrows():
        if True:
            
            tmp_DWCs = DWCDisplay.DWCDisplay( DWC1_x = branch_values["XDWC1"] , DWC1_y = branch_values["YDWC1"], DWC2_x = branch_values["XDWC2"] , DWC2_y = branch_values["YDWC2"], correction_x = correct_x, correction_y = correct_y )
            pds.loc[event_index,'predictX'], pds.loc[event_index,'predictY'] = tmp_DWCs.GetPredictCenter()
            
            tmp_list_S = []
            tmp_list_C = []
            
            for i in range(160):
                tmp_list_S.append( branch_values["SiPMPheS[160]["+str(i)+"]"] )
                tmp_list_C.append( branch_values["SiPMPheC[160]["+str(i)+"]"] )
            tmp_SiPMS =  SiPMDisplay.SiPMDisplay( {"S":tmp_list_S, "C":tmp_list_C} )
            
            pds.loc[event_index,'barycentreX'], pds.loc[event_index,'barycentreY'] = tmp_SiPMS.GetBarycentre(signal_type=signal_type)
            
            pds.loc[event_index,'barycentreWidthX'], pds.loc[event_index,'barycentreWidthY'] = tmp_SiPMS.GetBarycentreRMS(signal_type=signal_type)
            
            pds.loc[event_index,'barycentreX_S'], pds.loc[event_index,'barycentreY_S'] = tmp_SiPMS.GetBarycentre(signal_type="S")
            pds.loc[event_index,'barycentreX_C'], pds.loc[event_index,'barycentreY_C'] = tmp_SiPMS.GetBarycentre(signal_type="C")
            pds.loc[event_index,'barycentreWidthX_S'], pds.loc[event_index,'barycentreWidthY_S'] = tmp_SiPMS.GetBarycentreRMS(signal_type="S")
            pds.loc[event_index,'barycentreWidthX_C'], pds.loc[event_index,'barycentreWidthY_C'] = tmp_SiPMS.GetBarycentreRMS(signal_type="C")
    pds=pds[ pds["barycentreWidthX_S"] > 4 ]
    pds['barycentreX_CmS'] = pds['barycentreX_C'] - pds['barycentreX_S']
    pds['barycentreY_CmS'] = pds['barycentreY_C'] - pds['barycentreY_S']

    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [row number]", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsCaloY" )
    
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_C"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [C] [column number]", title=runnumber+" X Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloX_Cherenkov" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_C"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [C] [row number]", title=runnumber+" Y Cherenkov", outfile=output_prefix+runnumber+"_DWCvsCaloY_Cherenkov" )
    PlotUtils.draw2DScatter(pds["predictX"], pds["barycentreX_S"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [S] [column number]", title=runnumber+" X Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloX_Scintillator" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["barycentreY_S"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [S] [row number]", title=runnumber+" Y Scintillator", outfile=output_prefix+runnumber+"_DWCvsCaloY_Scintillator" )
    
    PlotUtils.drawTH1(pds["barycentreWidthX_C"], xname="barycentre WidthX [C]", yname="Event", title=runnumber+" WidthX [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Cherenkov" )
    PlotUtils.drawTH1(pds["barycentreWidthY_C"], xname="barycentre WidthY [C]", yname="Event", title=runnumber+" WidthY [C]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Cherenkov" )
    PlotUtils.drawTH1(pds["barycentreWidthX_S"], xname="barycentre WidthX [S]", yname="Event", title=runnumber+" WidthX [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX_Scintillator" )
    PlotUtils.drawTH1(pds["barycentreWidthY_S"], xname="barycentre WidthY [S]", yname="Event", title=runnumber+" WidthY [S]", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY_Scintillator" )
    
    PlotUtils.drawTH1(pds["barycentreX_CmS"], xname="C-S Barycenter CaloX [column number]", yname="Event", title=runnumber+" Barycenter X C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_CmS" )
    PlotUtils.drawTH1(pds["barycentreY_CmS"], xname="C-S Barycenter CaloY [row number]",    yname="Event", title=runnumber+" Barycenter Y C-S", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_CmS")
    
    PlotUtils.drawTH1(pds["barycentreX_C"], xname="Barycenter CaloX [C] [column number]", yname="Event", title=runnumber+" Barycenter X [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Cherenkov")
    PlotUtils.drawTH1(pds["barycentreY_C"], xname="Barycenter CaloY [C] [row number]",    yname="Event", title=runnumber+" Barycenter Y [C]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Cherenkov")
    PlotUtils.drawTH1(pds["barycentreX_S"], xname="Barycenter CaloX [S] [column number]", yname="Event", title=runnumber+" Barycenter X [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX_Scintillator")
    PlotUtils.drawTH1(pds["barycentreY_S"], xname="Barycenter CaloY [S] [row number]",    yname="Event", title=runnumber+" Barycenter Y [S]", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY_Scintillator")
    
    PlotUtils.draw2DScatterColored(pds["predictX"], pds["barycentreX"], pds["DeltaXDWC"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX_colored" )
    PlotUtils.draw2DScatterColored(pds["predictX"], pds["PShower"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X_colored" )
    
    PlotUtils.draw2DScatter(pds["predictX"], pds["PShower"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X" )
    PlotUtils.draw2DScatter(pds["predictY"], pds["PShower"], xname="Predict CaloY [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC1"], pds["PShower"], xname="DWC1 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC1vsPS_X" )
    PlotUtils.draw2DScatter(pds["YDWC1"], pds["PShower"], xname="DWC1 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC1vsPS_Y" )
    
    PlotUtils.draw2DScatter(pds["XDWC2"], pds["PShower"], xname="DWC2 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC2vsPS_X" )
    PlotUtils.draw2DScatter(pds["YDWC2"], pds["PShower"], xname="DWC2 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC2vsPS_Y" )
    
    PlotUtils.drawTH2(pds["predictX"], pds["predictY"], xname="Predict CaloX [mm]", yname="Predict CaloY [mm]", title=runnumber+" DWC prediction", 
            nbinsx=40, xstart=-20, xend=20, nbinsy=40, ystart=-20, yend=20, outfile=output_prefix+runnumber+"_DWCXvsDWCY")
    PlotUtils.drawTH2(pds["barycentreX"], pds["barycentreY"], xname="Barycenter CaloX [column number]", yname="Barycenter CaloY [row number]", title=runnumber+" Barycenter", 
            nbinsx=20, xstart=0, xend=20, nbinsy=20, ystart=0, yend=20, outfile=output_prefix+runnumber+"_CaloXvsCaloY")
    
    PlotUtils.drawTH1(pds["barycentreWidthX"], xname="barycentre WidthX", yname="Event", title=runnumber+" WidthX", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthX" )
    PlotUtils.drawTH1(pds["barycentreWidthY"], xname="barycentre WidthY", yname="Event", title=runnumber+" WidthY", nbins=50, xstart=0, xend=10, logY=True, outfile=output_prefix+runnumber+"_barycentreWidthY" )
    
    PlotUtils.drawTH1(pds["barycentreX"], xname="Barycenter CaloX [column number]", yname="Event", title=runnumber+" Barycenter X", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreX")
    PlotUtils.drawTH1(pds["barycentreY"], xname="Barycenter CaloY [row number]",    yname="Event", title=runnumber+" Barycenter Y", nbins=40, xstart=0, xend=20, logY=True, outfile=output_prefix+runnumber+"_barycentreY")
    PlotUtils.drawTH1(pds["predictX"], xname="Predict CaloX [mm]", yname="Event", title=runnumber+" CaloX", nbins=40, xstart=-20, xend=20, logY=True, outfile=output_prefix+runnumber+"_PredictCaloX" )
    PlotUtils.drawTH1(pds["predictY"], xname="Predict CaloY [mm]", yname="Event", title=runnumber+" CaloY", nbins=60, xstart=-30, xend=30, logY=True, outfile=output_prefix+runnumber+"_PredictCaloY" )



def runPMT(input_root_file, output_prefix, selectPreShower = False ):
    runnumber = input_root_file[-11:-5]
    # Open the root file
    root_file = uproot.open(input_root_file)
    # Get the keys (top-level objects) in the root file
    root_tree = root_file["Ftree/Events"]

    pds = root_tree.array(library="pd")
    
    correct_x, correct_y = correctDWCs(pds)
    
    pds=pds[ (pds["MCounter"] > 160) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] + pds["totSiPMCene"] > 0.1) ] # & (pds["PShower"] > 380)
    
    pds['totPMT1'] = pds['SPMT1'] + pds['CPMT1']
    pds['totPMT2'] = pds['SPMT2'] + pds['CPMT2']
    pds['totPMT3'] = pds['SPMT3'] + pds['CPMT3']
    pds['totPMT4'] = pds['SPMT4'] + pds['CPMT4']
    pds['totPMT5'] = pds['SPMT5'] + pds['CPMT5']
    pds['totPMT6'] = pds['SPMT6'] + pds['CPMT6']
    pds['totPMT7'] = pds['SPMT7'] + pds['CPMT7']
    pds['totPMT8'] = pds['SPMT8'] + pds['CPMT8']
    #registerPlotList(pds, runnumber, output_prefix+runnumber )
    pmt_dict = {}
    pmt_dict['1'] = pds['totPMT1'].mean()
    pmt_dict['2'] = pds['totPMT2'].mean()
    pmt_dict['3'] = pds['totPMT3'].mean()
    pmt_dict['4'] = pds['totPMT4'].mean()
    pmt_dict['5'] = pds['totPMT5'].mean()
    pmt_dict['6'] = pds['totPMT6'].mean()
    pmt_dict['7'] = pds['totPMT7'].mean()
    pmt_dict['8'] = pds['totPMT8'].mean()
    
    return pmt_dict

Degree = [2.5, 2, 1.5, 1, 0.5, 0, -0.5, -1, -2, -2.5]

#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run275.root", output_prefix = "90D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run278.root", output_prefix = "90D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run280.root", output_prefix = "90D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run282.root", output_prefix = "90D/")

#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run225.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run227.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run228.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run229.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run230.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run231.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run232.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run233.root", output_prefix = "0D/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run234.root", output_prefix = "0D/")

#runSiPM_LessThan380(input_root_file = NtuplePath+"physics_sps2023_run228.root", output_prefix = "Run228_LessThan380/")
#runSiPM_MoreThan380(input_root_file = NtuplePath+"physics_sps2023_run228.root", output_prefix = "Run228_MoreThan380/")

#runSiPM_LessThan380(input_root_file = NtuplePath+"physics_sps2023_run228.root", output_prefix = "Run228_DWC2LessThanM5/")
#runSiPM_MoreThan380(input_root_file = NtuplePath+"physics_sps2023_run228.root", output_prefix = "Run228_DWC2MoreThanM5/")

#runSiPM_LessThan380(input_root_file = NtuplePath+"physics_sps2023_run225.root", output_prefix = "Run225_RMSLessThan4/")
#runSiPM_LessThan380(input_root_file = NtuplePath+"physics_sps2023_run234.root", output_prefix = "Run234_RMSLessThan4/")
#runSiPM_LessThan380(input_root_file = NtuplePath+"physics_sps2023_run136.root", output_prefix = "Run136_RMSLessThan4/")
#runSiPM_LessThan380(input_root_file = NtuplePath+"physics_sps2023_run149.root", output_prefix = "Run149_RMSLessThan4/")
#runSiPM_MoreThan380(input_root_file = NtuplePath+"physics_sps2023_run225.root", output_prefix = "Run225_RMSMoreThan4/")

#xaxis = []
#for i in range(-8,9):
#    xaxis.append(i)
#
rms_x1,  rms_y1 , fraction1  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run225.root", output_prefix = "DWC_SiPM_Match/")
rms_x2,  rms_y2 , fraction2  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run227.root", output_prefix = "DWC_SiPM_Match/")
rms_x3,  rms_y3 , fraction3  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run202.root", output_prefix = "DWC_SiPM_Match/")
rms_x4,  rms_y4 , fraction4  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run228.root", output_prefix = "DWC_SiPM_Match/")
rms_x5,  rms_y5 , fraction5  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run229.root", output_prefix = "DWC_SiPM_Match/")
rms_x6,  rms_y6 , fraction6  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run230.root", output_prefix = "DWC_SiPM_Match/")
rms_x7,  rms_y7 , fraction7  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run231.root", output_prefix = "DWC_SiPM_Match/")
rms_x8,  rms_y8 , fraction8  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run232.root", output_prefix = "DWC_SiPM_Match/")
rms_x9,  rms_y9 , fraction9  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run233.root", output_prefix = "DWC_SiPM_Match/")
rms_x10, rms_y10, fraction10 = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run234.root", output_prefix = "DWC_SiPM_Match/")
#
#rms_x = [
#rms_x1, rms_x2, rms_x3, rms_x4, rms_x5, rms_x6, rms_x7, rms_x8, rms_x9, rms_x10,
#]
#rms_y = [
#rms_y1 ,rms_y2 ,rms_y3 ,rms_y4 ,rms_y5 ,rms_y6 ,rms_y7 ,rms_y8 ,rms_y9 ,rms_y10,
#]
#fraction = [
#fraction1 ,fraction2 ,fraction3 ,fraction4 ,fraction5 ,fraction6 ,fraction7 ,fraction8 ,fraction9 ,fraction10,
#]
#PlotUtils.draw2DScatter(Degree, rms_x, "degree", "widthX", "No_PS", size=0.8)
#PlotUtils.draw2DScatter(Degree, rms_y, "degree", "widthY", "No_PS", size=0.8)

#for datafile in [ 
#    "physics_sps2023_run275.root",
#    "physics_sps2023_run278.root",
#    "physics_sps2023_run280.root",
#    "physics_sps2023_run282.root"]:
#    SiPMEnergy,PMTEnergy = runOneFile(input_root_file = NtuplePath+datafile, output_prefix = "90D_data/")


#SiPM = []
#PMT_total = []
#Degree = [2.5, 2, 1.5, 1, 0.5, 0, -0.5, -1, -2, -2.5]
#for datafile in [ 
#    "physics_sps2023_run225.root",
#    "physics_sps2023_run227.root",
#    "physics_sps2023_run202.root",
#    "physics_sps2023_run228.root",
#    "physics_sps2023_run229.root",
#    "physics_sps2023_run230.root",
#    "physics_sps2023_run231.root",
#    "physics_sps2023_run232.root",
#    "physics_sps2023_run233.root",
#    "physics_sps2023_run234.root"]:
#    
#    SiPMEnergy,PMTEnergy = runOneFile(input_root_file = NtuplePath+datafile, output_prefix = "0D_data/")
#    SiPM.append(SiPMEnergy)
#    PMT_total.append(PMTEnergy)
#print(SiPM)
#print(Degree)
#PlotUtils.draw2DScatter(Degree, SiPM, "degree", "totSiPM", "No PS", size=0.8)

#PMT = {}
#for i in range(1,9,1):
#    PMT[str(i)] = []
#Degree = [2.5, 2, 1.5, 1, 0.5, 0, -0.5, -1, -2, -2.5]
#for datafile in [ 
#    "physics_sps2023_run225.root",
#    "physics_sps2023_run227.root",
#    "physics_sps2023_run202.root",
#    "physics_sps2023_run228.root",
#    "physics_sps2023_run229.root",
#    "physics_sps2023_run230.root",
#    "physics_sps2023_run231.root",
#    "physics_sps2023_run232.root",
#    "physics_sps2023_run233.root",
#    "physics_sps2023_run234.root"]:
#    
#    PMTEnergy = runPMT(input_root_file = NtuplePath+datafile, output_prefix = "0D_data/")
#    for i in range(1,9,1):
#        PMT[str(i)].append(PMTEnergy[str(i)])
##print(SiPM)
##print(Degree)
#tmp_x_degree=[]
#tmp_y_value =[]
#tmp_c_index =[]
#for i in range(1,9,1):
#    PlotUtils.draw2DScatter(Degree, PMT[str(i)], "degree", "PMT"+str(i), "No PS PMT"+str(i), size=0.8, outfile = "PMT_"+str(i)  )
#for angle in range(len(Degree)):
#    PMT_eachAngle_list=[]
#    for i in range(1,9,1):
#        PMT_eachAngle_list.append(PMT[str(i)][angle])
#    a = PMTDisplay.PMTDisplay(PMT_eachAngle_list, SiPM=SiPM[angle])
#    a.draw( title = "PMT @ "+str(Degree[angle])+" Degree", filename= "PMT_"+str(Degree[angle]) )

#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run245.root", output_prefix = "Pion/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run246.root", output_prefix = "Pion/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run247.root", output_prefix = "Pion/")


#Degree = [2.5,0,1,-1]
#rms_x1,  rms_y1 , fraction1  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run136.root", output_prefix = "DWC_SiPM_Match_Run1xx/")
#rms_x2,  rms_y2 , fraction2  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run145.root", output_prefix = "DWC_SiPM_Match_Run1xx/")
#rms_x3,  rms_y3 , fraction3  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run146.root", output_prefix = "DWC_SiPM_Match_Run1xx/")
#rms_x4,  rms_y4 , fraction4  = runSiPM(input_root_file = NtuplePath+"physics_sps2023_run149.root", output_prefix = "DWC_SiPM_Match_Run1xx/")
#rms_x = [
#rms_x1, rms_x2, rms_x3, rms_x4, 
#]
#rms_y = [
#rms_y1 ,rms_y2 ,rms_y3 ,rms_y4 ,
#]
#PlotUtils.draw2DScatter(Degree, rms_x, "degree", "widthX", "No_PS", size=0.8)
#PlotUtils.draw2DScatter(Degree, rms_y, "degree", "widthY", "No_PS", size=0.8)


#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run252.root", output_prefix = "DWC_SiPM_Match_Ele/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run255.root", output_prefix = "DWC_SiPM_Match_Ele/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run258.root", output_prefix = "DWC_SiPM_Match_Ele/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run266.root", output_prefix = "DWC_SiPM_Match_Ele/")
#runSiPM(input_root_file = NtuplePath+"physics_sps2023_run269.root", output_prefix = "DWC_SiPM_Match_Ele/")

""" Draw Simulation 
    Position_x, y, z, /tbgeo/xshift, /tbgeo/yshift, /tbgeo/horizrot, /tbgeo/vertrot 
"""

#runSimulation(input_root_file = NtuplePath_Simulation + "DRTB23Sim_Run0_m2.root", output_prefix = "./Simulation/m2deg_")
#runSimulation(input_root_file = NtuplePath_Simulation + "DRTB23Sim_Run1_2.root", output_prefix = "./Simulation/p2deg_")
#runSimulation(input_root_file = NtuplePath_Simulation + "DRTB23Sim_Run0_offset.root", output_prefix = "./Simulation/offset3cm_p2deg_")
#runSimulation(input_root_file = NtuplePath_Simulation + "DRTB23Sim_Run0.root", output_prefix = "./Simulation/nom_")

#barycentreX = []
#barycentreY = []
#gpsX = []
#gpsY = []
#barycentreX_0, barycentreY_0 = runSimulation(input_root_file = "/afs/cern.ch/work/c/caiy/public/FCC/Simulation/output_2deg/" + "DRTB23Sim_Run0.root", output_prefix = "./Simulation2deg/0_0_m55_0_2_0_")
#barycentreX_1, barycentreY_1 = runSimulation(input_root_file = "/afs/cern.ch/work/c/caiy/public/FCC/Simulation/output_2deg/" + "DRTB23Sim_Run1.root", output_prefix = "./Simulation2deg/0_1_m55_0_2_0_")
#barycentreX_2, barycentreY_2 = runSimulation(input_root_file = "/afs/cern.ch/work/c/caiy/public/FCC/Simulation/output_2deg/" + "DRTB23Sim_Run2.root", output_prefix = "./Simulation2deg/1_0_m55_0_2_0_")
#barycentreX_3, barycentreY_3 = runSimulation(input_root_file = "/afs/cern.ch/work/c/caiy/public/FCC/Simulation/output_2deg/" + "DRTB23Sim_Run3.root", output_prefix = "./Simulation2deg/0_m1_m55_0_2_0_")
#barycentreX_4, barycentreY_4 = runSimulation(input_root_file = "/afs/cern.ch/work/c/caiy/public/FCC/Simulation/output_2deg/" + "DRTB23Sim_Run4.root", output_prefix = "./Simulation2deg/m1_0_m55_0_2_0_")
#
#barycentreX = barycentreX_0 + barycentreX_1 + barycentreX_2 + barycentreX_3 + barycentreX_4
#barycentreY = barycentreY_0 + barycentreY_1 + barycentreY_2 + barycentreY_3 + barycentreY_4
#gpsX = [0] * len(barycentreX_0) + [0] * len(barycentreX_1) + [1] * len(barycentreX_2) + [0] * len(barycentreX_3) + [-1] * len(barycentreX_4)
#gpsY = [0] * len(barycentreY_0) + [1] * len(barycentreY_1) + [0] * len(barycentreY_2) + [-1] * len(barycentreY_3) + [0] * len(barycentreY_4)
#PlotUtils.draw2DScatter(gpsX, barycentreX, xname="GPS CaloX [mm]", yname="Barycenter CaloX [column number]", title=" 2 Deg X", outfile="./Simulation2deg/DWCvsCaloX" )
#PlotUtils.draw2DScatter(gpsY, barycentreY, xname="GPS CaloY [mm]", yname="Barycenter CaloY [row number]", title=" 2 Deg Y", outfile="./Simulation2deg/DWCvsCaloY" )
#
#barycentreX = []
#barycentreY = []
#gpsX = []
#gpsY = []
#barycentreX_0, barycentreY_0 = runSimulation(input_root_file = NtuplePath_Simulation+"DRTB23Sim_Run0.root", output_prefix = "./Simulationm2deg/0_0_m55_0_m2_0_")
#barycentreX_1, barycentreY_1 = runSimulation(input_root_file = NtuplePath_Simulation+"DRTB23Sim_Run1.root", output_prefix = "./Simulationm2deg/0_1_m55_0_m2_0_")
#barycentreX_2, barycentreY_2 = runSimulation(input_root_file = NtuplePath_Simulation+"DRTB23Sim_Run2.root", output_prefix = "./Simulationm2deg/1_0_m55_0_m2_0_")
#barycentreX_3, barycentreY_3 = runSimulation(input_root_file = NtuplePath_Simulation+"DRTB23Sim_Run3.root", output_prefix = "./Simulationm2deg/0_m1_m55_0_m2_0_")
#barycentreX_4, barycentreY_4 = runSimulation(input_root_file = NtuplePath_Simulation+"DRTB23Sim_Run4.root", output_prefix = "./Simulationm2deg/m1_0_m55_0_m2_0_")
#
#barycentreX = barycentreX_0 + barycentreX_1 + barycentreX_2 + barycentreX_3 + barycentreX_4
#barycentreY = barycentreY_0 + barycentreY_1 + barycentreY_2 + barycentreY_3 + barycentreY_4
#gpsX = [0] * len(barycentreX_0) + [0] * len(barycentreX_1) + [1] * len(barycentreX_2) + [0] * len(barycentreX_3) + [-1] * len(barycentreX_4)
#gpsY = [0] * len(barycentreY_0) + [1] * len(barycentreY_1) + [0] * len(barycentreY_2) + [-1] * len(barycentreY_3) + [0] * len(barycentreY_4)
#PlotUtils.draw2DScatter(gpsX, barycentreX, xname="GPS CaloX [mm]", yname="Barycenter CaloX [column number]", title=" -2 Deg X", outfile="./Simulationm2deg/DWCvsCaloX" )
#PlotUtils.draw2DScatter(gpsY, barycentreY, xname="GPS CaloY [mm]", yname="Barycenter CaloY [row number]", title=" -2 Deg Y", outfile="./Simulationm2deg/DWCvsCaloY" )


def DegreeToRadian(degree):
    return abs(math.pi * degree/180)


def calMuonEnergy(theta_xy, theta_z, alpha, calorimeter_length, Start , End ):
    P_0 = 1
    Delta_f = 1
    P_0 = Delta_f * alpha
    tan2 = math.tan(theta_xy) * math.tan(theta_xy) + math.tan(theta_z) * math.tan(theta_z)
    sin2 = tan2/(tan2+1)
    E = (P_0/Delta_f) * (math.sqrt(tan2)/alpha) * ( math.exp( -1*(alpha/math.sqrt(tan2)) * Start ) - math.exp( -1*(alpha/math.sqrt(tan2)) * End ) ) * ( 1 / math.sqrt(sin2) )
    return E


def calMuonEnergyCenter(theta_xy, theta_z, alpha, calorimeter_length, Start , End ):
    P_0 = 1
    Delta_f = 1
    P_0 = Delta_f * alpha
    tan2 = math.tan(theta_xy) * math.tan(theta_xy) + math.tan(theta_z) * math.tan(theta_z)
    sin2 = tan2/(tan2+1)
    de = ( math.exp( -1*(alpha/math.sqrt(tan2)) * Start ) - math.exp( -1*(alpha/math.sqrt(tan2)) * End ) )
    num_start = Start * math.exp( -1*(alpha/math.sqrt(tan2)) * Start ) + math.sqrt(tan2)/alpha * math.exp( -1*(alpha/math.sqrt(tan2)) * Start )
    num_end = End * math.exp( -1*(alpha/math.sqrt(tan2)) * End ) + math.sqrt(tan2)/alpha * math.exp( -1*(alpha/math.sqrt(tan2)) * End )
    U = ( num_start - num_end ) / de
    return U


#beam_detector_angle = np.arange( -3, 3, 0.1)
#particle_angle_xy = 0
#particle_angle_z  = 0
#average_Es = []
#for each_beam_detector_angle in beam_detector_angle:
#    total_Es = 0
#    for i in range(1000):
#        particle_angle_xy = random.random()
#        particle_angle_z = random.random()
#        if abs(particle_angle_xy+each_beam_detector_angle) > 2.8:
#            total_Es += calMuonEnergy( DegreeToRadian(particle_angle_xy+each_beam_detector_angle), DegreeToRadian(0), 0.0005, 1000, (1000*math.tan(DegreeToRadian(particle_angle_xy+each_beam_detector_angle))-1000*math.tan(DegreeToRadian(2.8))), 1000*math.tan(DegreeToRadian(particle_angle_xy+each_beam_detector_angle)) )
#        else:
#            total_Es += calMuonEnergy( DegreeToRadian(particle_angle_xy+each_beam_detector_angle), DegreeToRadian(0), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(particle_angle_xy+each_beam_detector_angle)) )
#    total_Es /= 1000
#    average_Es.append(total_Es)
#
#PlotUtils.draw2DScatter( beam_detector_angle, average_Es, "Degree", "E_s", "naive_model" )

#xAxis_low = np.arange( -5.7, -2.8, 0.1)
#print(xAxis_low)
#xAxis=np.arange( -2.8, 2.9, 0.1)
#print(xAxis)
#xAxis_high = np.arange( 2.9, 5.8, 0.1)
#print(xAxis_high)
#yAxis=[]
#for i in xAxis_low:
#    yAxis.append( calMuonEnergy( DegreeToRadian(i), DegreeToRadian(0.001), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(2.8)) ) )
#for i in xAxis:
#    yAxis.append( calMuonEnergy( DegreeToRadian(i), DegreeToRadian(0.001), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(i)) ) )
#for i in xAxis_high:
#    yAxis.append( calMuonEnergy( DegreeToRadian(i), DegreeToRadian(0.001), 0.0005, 1000, (1000*math.tan(DegreeToRadian(i))-1000*math.tan(DegreeToRadian(2.8))), 1000*math.tan(DegreeToRadian(i)) ) )

#print(1000*math.tan(DegreeToRadian(2.8)))
#print( calMuonEnergy( DegreeToRadian(2.8), DegreeToRadian(0), 0.0005, 1000) )
#print( calMuonEnergy( DegreeToRadian(2.8), DegreeToRadian(0), 0.0005, 1000, 1000*math.tan(DegreeToRadian(4.9))-1000*math.tan(DegreeToRadian(2.8))   ) )
#
#print(  1000*math.tan(DegreeToRadian(4.9)) , 1000*math.tan(DegreeToRadian(4.9))-1000*math.tan(DegreeToRadian(2.8)) )

#PlotUtils.draw2DScatter( np.concatenate((xAxis_low,xAxis,xAxis_high)), yAxis, "Degree", "E_s", "Scintillation_correction" )


#xAxis_low = np.arange( -5.7, -2.8, 0.1)
#print(xAxis_low)
#xAxis_1=np.arange( -2.8, 0, 0.1)
#print(xAxis_1)
#xAxis_2=np.arange( 0.1, 2.9, 0.1)
#print(xAxis_2)
#xAxis_high = np.arange( 2.9, 5.8, 0.1)
#print(xAxis_high)
#yAxis=[]
#for i in xAxis_low:
#    yAxis.append( calMuonEnergyCenter( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(2.8)) ) )
#for i in xAxis_1:
#    yAxis.append( calMuonEnergyCenter( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(i)) ) )
#for i in xAxis_2:
#    yAxis.append( 1000*math.tan(DegreeToRadian(i)) - calMuonEnergyCenter( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(i)) ) )
#for i in xAxis_high:
#    yAxis.append( 1000*math.tan(DegreeToRadian(i)) - calMuonEnergyCenter( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, (1000*math.tan(DegreeToRadian(i))-1000*math.tan(DegreeToRadian(2.8))), 1000*math.tan(DegreeToRadian(i)) ) )
#
#PlotUtils.draw2DScatter( np.concatenate((xAxis_low,xAxis_1,xAxis_2,xAxis_high)), yAxis, "Degree", "Derivate_From_Beam_Center", "Scintillation" )


#xAxis_low = np.arange( 2.8, 5.8, 0.1)
#print(xAxis_low)
#xAxis=np.arange( 5.8, 7.0, 0.1)
#print(xAxis)
#xAxis_high = np.arange( 2.8, 7.0, 0.1)
#print(xAxis_high)
#yAxis=[]
#for i in xAxis_low:
#    yAxis.append( calMuonEnergy( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(i)) ) )
#for i in xAxis:
#    yAxis.append( calMuonEnergy( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 1000/2*math.tan(DegreeToRadian(i))-1000/2*math.tan(DegreeToRadian(5.7)) , 1000/2*math.tan(DegreeToRadian(i))+1000/2*math.tan(DegreeToRadian(5.7)) ) )
#for i in xAxis_high:
#    print( i, 1000/2*math.tan(DegreeToRadian(i))-1000/2*math.tan(DegreeToRadian(5.7)) , 1000/2*math.tan(DegreeToRadian(i))+1000/2*math.tan(DegreeToRadian(5.7)), calMuonEnergy( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 1000/2*math.tan(DegreeToRadian(i))-1000/2*math.tan(DegreeToRadian(5.7)) , 1000/2*math.tan(DegreeToRadian(i))+1000/2*math.tan(DegreeToRadian(5.7)) ) )
#PlotUtils.draw2DScatter( np.concatenate((xAxis_low,xAxis)), yAxis, "Degree", "E_s", "Scintillation_HighAngle" )
#
#print( 1000/2*math.tan(DegreeToRadian(5.7))  , 1000*math.tan(DegreeToRadian(2.8)) )


#xAxis_low = np.arange( 2.8, 5.8, 0.1)
#print(xAxis_low)
#xAxis=np.arange( 5.8, 7.0, 0.1)
#print(xAxis)
#xAxis_high = np.arange( 2.8, 7.0, 0.1)
#print(xAxis_high)
#yAxis=[]
#for i in xAxis_low:
#    yAxis.append( 1000/2*math.tan(DegreeToRadian(i)) - calMuonEnergyCenter( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 0, 1000*math.tan(DegreeToRadian(i)) ) )
#for i in xAxis:
#    yAxis.append( abs(1000/2*math.tan(DegreeToRadian(i)) - calMuonEnergyCenter( DegreeToRadian(i), DegreeToRadian(0), 0.0005, 1000, 1000/2*math.tan(DegreeToRadian(i))-1000/2*math.tan(DegreeToRadian(5.7)) , 1000/2*math.tan(DegreeToRadian(i))+1000/2*math.tan(DegreeToRadian(5.7)) )) )
#
#PlotUtils.draw2DScatter( np.concatenate((xAxis_low,xAxis)), yAxis, "Degree", "Derivate_From_Beam_Center", "Scintillation_HighAngle" )
