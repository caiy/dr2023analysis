import uproot
import matplotlib.pyplot as plt
import matplotlib.colors as matcolors
import numpy as np
import ROOT
from array import array
import math
import warnings
import pandas as pd

# Copied from @ANDREA 
sq3=1.732
sq3m1=sq3/3.
d1 = 1740.     # distance in mm from DWC1 to DWC2
d2 = 2475.     # distance in mm from DWC2 to calorimeter
dPS = 175.     # distance in mm from calorimeter to PreShower
NofFibersrow = 20   # number of rows in one module
NofFiberscolumn = 16     # number of fiber columns in one module
tuberadius = 1.0
dtubeY=sq3*tuberadius                                       # dtubeY given in simulation before rotation
dtubeX=2.*tuberadius
moduleX = (2*NofFiberscolumn+1)*tuberadius
moduleY = (NofFibersrow-1.)*dtubeY+4.*tuberadius*sq3m1

NofFibersrow2 = NofFibersrow/2
tuberadius2 = 2*tuberadius
tuberadiusmoduleX2 = tuberadius - moduleX/2
dtubeY2 = tuberadius*(2*sq3m1) - moduleY/2 

class SiPMDisplay:
    def __init__(self, readout_value = {}, simulation = False ):
        allowed_signal_type = ["C","S"]
        self.simulation = simulation
        
        self.bary_centre_x = -1000
        self.bary_centre_y = -1000
        self.bary_centre_row = -1000
        self.bary_centre_col = -1000
        
        # Initialize dataframe, each row is a fibre.
        self.readout_value = readout_value
        self.signal_type = list(readout_value.keys())
        df_input_signal_type = []
        df_input_SiPM_value = []
        df_input_index = []
        for each_signal_type in self.signal_type:
            if each_signal_type in allowed_signal_type:
                df_input_SiPM_value += self.readout_value[each_signal_type]
                df_input_signal_type += [each_signal_type]*len(self.readout_value[each_signal_type])
                df_input_index += range(len(self.readout_value[each_signal_type]))
            else:
                raise TypeError("Only ",allowed_signal_type," are allowed")
        #print(df_input_signal_type, df_input_SiPM_value, df_input_index)
        self.SiPM_type = np.array(df_input_signal_type)
        self.SiPM_value = np.array(df_input_SiPM_value)
        self.SiPM_index = np.array(df_input_index)
        
        #if simulation :
        #    def _wrap(index: int, signal_type: str):
        #        return self.ApplySiPMGeoConverter_G4(index, signal_type)
        #else :
        #    def _wrap(index: int, signal_type: str):
        #        return self.ApplySiPMGeoConverter(index, signal_type)
        if simulation :
            self.col_row_x_y_label = np.transpose( 
                                              np.array(list( map(self.ApplySiPMGeoConverter_G4, self.SiPM_index, self.SiPM_type) )) 
                                              )
        else :
            self.col_row_x_y_label = np.transpose( 
                                                  np.array(list( map(self.ApplySiPMGeoConverter, self.SiPM_index, self.SiPM_type) )) 
                                                  )
        self.SiPM_col = self.col_row_x_y_label[0]
        self.SiPM_row = self.col_row_x_y_label[1]
        self.SiPM_x = self.col_row_x_y_label[2]
        self.SiPM_y = self.col_row_x_y_label[3]
        self.SiPM_label = self.col_row_x_y_label[4]
        #print( self.col_row_x_y_label ) # 'ColID', 'RowID', 'SiPM_X', 'SiPM_Y', 'Label'
        """
            C : 0
            S : 1
        """
        #self.df[['ColID', 'RowID', 'SiPM_X', 'SiPM_Y', 'Label']] = self.df.apply(lambda x: _wrap(x.SiPMID, x.SignalType), axis=1, result_type="expand")
        #self.value_plot = np.zeros((NofFibersrow,NofFiberscolumn)) #[ [0]*NofFiberscolumn for i in range(int(NofFibersrow))]
        #self.index_plot = np.zeros((NofFibersrow,NofFiberscolumn)) #[ [0]*NofFiberscolumn for i in range(int(NofFibersrow))]
        #self.value_list = []
        #self.index_list = []
        #self.xPosi_list = []
        #self.yPosi_list = []
        #
        #if simulation :
        #    if self.readout_value and len(self.signal_type) > 0 :
        #        #print("YES")
        #        for each_signal_type in self.signal_type:
        #            self.RunSiPMGeoConverter_G4(self.readout_value[each_signal_type], each_signal_type)
        #else :
        #    if self.readout_value and len(self.signal_type) > 0 :
        #        #print("YES")
        #        for each_signal_type in self.signal_type:
        #            self.RunSiPMGeoConverter(self.readout_value[each_signal_type], each_signal_type)
    
    
    #def update(self):
    #    self.signal_type = list(self.readout_value.keys())
    #    if self.readout_value and len(self.signal_type) > 0 :
    #        #print("YES")
    #        for each_signal_type in self.signal_type:
    #            self.RunSiPMGeoConverter(self.readout_value[each_signal_type], each_signal_type)
    
    
    def ApplySiPMGeoConverter(self, index: int, signal_type: str):
        """
        TBDataPreparation/2023_SPS/scripts/PhysicsEvent.h
        for(uint16_t i=0;i<320;++i){
            int column=i%16;
            int row=i/16;
            int ind=(row/2)*16+column;
            if(row % 2 == 0)evout->SiPMPheC[ind]=0;
            if(row % 2 == 1)evout->SiPMPheS[ind]=0;
        }
             ^Y(G4)       Z⊕(G4)
             |   row-0 C-0
             | row-1 S-0
             |   row-2 C-1
             | row-3 S-1
            <-----------X(G4)
        """
        colID = int( index%(NofFiberscolumn)      )
        rowID = int( 2*( index//(NofFiberscolumn) ) )
        SiPM_X = (tuberadius2)*colID + tuberadiusmoduleX2 
        SiPM_Y = dtubeY *( NofFibersrow - rowID ) + dtubeY2
        label=0

        if signal_type == "S" : # even rows: row%2==0
            SiPM_Y = SiPM_Y - dtubeY
            rowID += 1
            label = 1
        elif signal_type == "C" :
            SiPM_X = SiPM_X + tuberadius
        return colID, rowID, SiPM_X, SiPM_Y, label
    
    
    def RunSiPMGeoConverter(self, readout_value: list, signal_type: str):
        """
        TBDataPreparation/2023_SPS/scripts/PhysicsEvent.h
        for(uint16_t i=0;i<320;++i){
            int column=i%16;
            int row=i/16;
            int ind=(row/2)*16+column;
            if(row % 2 == 0)evout->SiPMPheC[ind]=0;
            if(row % 2 == 1)evout->SiPMPheS[ind]=0;
        }
             ^Y(G4)       Z⊕(G4)
             |   row-0 C-0
             | row-1 S-0
             |   row-2 C-1
             | row-3 S-1
            <-----------X(G4)
        """
        if len(readout_value) == 0 :
            warnings.warn(f"Input readout list {readout_value} is empty...........Return 0")
            return [0]
        else:
            for index in range(len(readout_value)):
                array_position, physical_position = self.SiPMGeoConverter(index, signal_type)
                #print(array_position)
                #print(physical_position)
                if signal_type == "S" :
                    self.index_plot[array_position[1]+1,array_position[0]] = index
                    self.value_plot[array_position[1]+1,array_position[0]] = readout_value[index]
                    self.index_list.append("S-"+str(index))
                elif signal_type == "C" :
                    self.index_plot[array_position[1],array_position[0]] = index
                    self.value_plot[array_position[1],array_position[0]] = readout_value[index]
                    self.index_list.append("C-"+str(index))
                self.value_list.append(readout_value[index])
                self.xPosi_list.append(physical_position[0])
                self.yPosi_list.append(physical_position[1])
    
    
    def ApplySiPMGeoConverter_G4(self, index: int, signal_type: str):
        """
        for(int row=0; row<NofFibersrow; row++){
            copynumber = ((NofFibersrow/2)*column+row/2);
            if(row%2==0){
                S_x = moduleX/2 - tuberadius - (tuberadius*2+2*tolerance)*column;
                S_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance*mm)*(row)+tuberadius*(2.*sq3m1-1.);
                S_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance)*row+tuberadius*(2.*sq3m1-1.);
            }
            if(row%2 != 0){
                C_x = moduleX/2 - tuberadius - tuberadius - (tuberadius*2+2*tolerance)*column;
                C_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance)*row+tuberadius*(2.*sq3m1-1.);
            }
        }
             ^Y(G4)       Z⊕(G4)
             |   row-3 C-1
             | row-2 S-1
             |   row-1 C-0
             | row-0 S-0
            <-----------X(G4)
        """
        
        colID = int( index//(NofFibersrow2)      )
        rowID = int( 2*( index%(NofFibersrow2) ) )
        SiPM_X = tuberadius2*colID + tuberadiusmoduleX2
        SiPM_Y = dtubeY*rowID + dtubeY2
        label=0

        if signal_type == "S" :
            label =  1
        elif signal_type == "C" :
            SiPM_X = SiPM_X + tuberadius
            SiPM_Y = SiPM_Y + dtubeY
            rowID += 1
        return colID, rowID, SiPM_X, SiPM_Y, label
    
    
    def RunSiPMGeoConverter_G4(self, readout_value: list, signal_type: str):
        """
        for(int row=0; row<NofFibersrow; row++){
            copynumber = ((NofFibersrow/2)*column+row/2);
            if(row%2==0){
                S_x = moduleX/2 - tuberadius - (tuberadius*2+2*tolerance)*column;
                S_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance*mm)*(row)+tuberadius*(2.*sq3m1-1.);
                S_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance)*row+tuberadius*(2.*sq3m1-1.);
            }
            if(row%2 != 0){
                C_x = moduleX/2 - tuberadius - tuberadius - (tuberadius*2+2*tolerance)*column;
                C_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance)*row+tuberadius*(2.*sq3m1-1.);
            }
        }
             ^Y(G4)       Z⊕(G4)
             |   row-3 C-1
             | row-2 S-1
             |   row-1 C-0
             | row-0 S-0
            <-----------X(G4)
        """
        if len(readout_value) == 0 :
            warnings.warn(f"Input readout list {readout_value} is empty...........Return 0")
            return [0]
        else:
            for index in range(len(readout_value)):
                array_position, physical_position = self.SiPMGeoConverter_G4(index, signal_type)
                #print(array_position)
                #print(physical_position)
                if signal_type == "S" :
                    self.index_plot[array_position[1],array_position[0]] = index
                    self.value_plot[array_position[1],array_position[0]] = readout_value[index]
                    self.index_list.append("S-"+str(index))
                elif signal_type == "C" :
                    self.index_plot[array_position[1]+1,array_position[0]] = index
                    self.value_plot[array_position[1]+1,array_position[0]] = readout_value[index]
                    self.index_list.append("C-"+str(index))
                self.value_list.append(readout_value[index])
                self.xPosi_list.append(physical_position[0])
                self.yPosi_list.append(physical_position[1])
    
    
    #def NumberOfActiveBoard(self):
    #    act = 0
    #    for i in range(0,NofFibersrow,4):
    #        if sum(self.value_plot[i]) > 0 or sum(self.value_plot[i+1]) > 0 or sum(self.value_plot[i+2]) > 0 or sum(self.value_plot[i+3]) > 0:
    #            act += 1
    #    return act
    #
    #
    #def getActiveBoard(self):
    #    act = []
    #    for i in range(0,NofFibersrow,4):
    #        if sum(self.value_plot[i]) > 0 or sum(self.value_plot[i+1]) > 0 or sum(self.value_plot[i+2]) > 0 or sum(self.value_plot[i+3]) > 0:
    #            act.append([self.value_plot[i], self.value_plot[i+1], self.value_plot[i+2], self.value_plot[i+3]])
    #    return act
    #
    #
    #def PeakPosition(self, order = 1):
    #    low_to_high = np.dstack(np.unravel_index(np.argsort(self.value_plot.ravel()), (NofFibersrow,NofFiberscolumn)))
    #    orderlist = low_to_high[0]
    #    index  = int( NofFibersrow * NofFiberscolumn - order )
    #    #print(index)
    #    return orderlist[index][1], orderlist[index][0]
    
    
    def GetBarycentre(self, signal_type = 'ALL'):
        if signal_type not in ['ALL', 'S', 'C']:
            warnings.warn(f"signal_type {signal_type} is not in 'ALL', 'S', 'C'...........Change to ALL ")
        if self.bary_centre_x > -900 and self.bary_centre_y > -900 :
            return self.bary_centre_x, self.bary_centre_y
        else:
            x = 0
            y = 0
            if signal_type == 'ALL':
                WeightedSiPM_X = self.SiPM_x * self.SiPM_value
                WeightedSiPM_Y = self.SiPM_y * self.SiPM_value
                x = np.sum(WeightedSiPM_X) / np.sum(self.SiPM_value)
                y = np.sum(WeightedSiPM_Y) / np.sum(self.SiPM_value)
            elif signal_type == 'S':
                WeightedValue = self.SiPM_value * self.SiPM_label
                WeightedSiPM_X = self.SiPM_x * WeightedValue
                WeightedSiPM_Y = self.SiPM_y * WeightedValue
                x = np.sum(WeightedSiPM_X) / np.sum(WeightedValue)
                y = np.sum(WeightedSiPM_Y) / np.sum(WeightedValue)
            elif signal_type == 'C':
                Weightedlabel = self.SiPM_label - 1
                WeightedValue = self.SiPM_value * Weightedlabel
                WeightedSiPM_X = self.SiPM_x * WeightedValue
                WeightedSiPM_Y = self.SiPM_y * WeightedValue
                x = np.sum(WeightedSiPM_X) / np.sum(WeightedValue)
                y = np.sum(WeightedSiPM_Y) / np.sum(WeightedValue)

            self.bary_centre_x = x
            self.bary_centre_y = y
            return x,y
        """
        temp_sumX = 0
        temp_sumY = 0
        temp_total_energy = 0
        if signal_type == 'ALL':
            for i in range(NofFibersrow):
                for j in range(NofFiberscolumn):
                    if self.value_plot[i,j] <= 0 :
                        temp_total_energy += 0
                        temp_sumX += j * 0
                        temp_sumY += i * 0
                    else:
                        temp_total_energy += self.value_plot[i,j]
                        temp_sumX += j * self.value_plot[i,j]
                        temp_sumY += i * self.value_plot[i,j]
        elif signal_type == 'S':
            for i in range(1,NofFibersrow,2):
                for j in range(NofFiberscolumn):
                    if self.value_plot[i,j] <= 0 :
                        temp_total_energy += 0
                        temp_sumX += j * 0
                        temp_sumY += i * 0
                    else:
                        temp_total_energy += self.value_plot[i,j]
                        temp_sumX += j * self.value_plot[i,j]
                        temp_sumY += i * self.value_plot[i,j]
        elif signal_type == 'C':
            for i in range(0,NofFibersrow,2):
                for j in range(NofFiberscolumn):
                    if self.value_plot[i,j] <= 0 :
                        temp_total_energy += 0
                        temp_sumX += j * 0
                        temp_sumY += i * 0
                    else:
                        temp_total_energy += self.value_plot[i,j]
                        temp_sumX += j * self.value_plot[i,j]
                        temp_sumY += i * self.value_plot[i,j]
        return temp_sumX/temp_total_energy , temp_sumY/temp_total_energy
        """
    
    
    def GetBarycentreRowCol(self, signal_type = 'ALL'):
        if signal_type not in ['ALL', 'S', 'C']:
            warnings.warn(f"signal_type {signal_type} is not in 'ALL', 'S', 'C'...........Change to ALL ")
        if self.bary_centre_col > -900 and self.bary_centre_row > -900 :
            return self.bary_centre_x, self.bary_centre_y
        else:
            col = 0
            row = 0
            if signal_type == 'ALL':
                WeightedSiPM_row = self.SiPM_row * self.SiPM_value
                WeightedSiPM_col = self.SiPM_col * self.SiPM_value
                x = np.sum(WeightedSiPM_row) / np.sum(self.SiPM_value)
                y = np.sum(WeightedSiPM_col) / np.sum(self.SiPM_value)
            elif signal_type == 'S':
                WeightedValue = self.SiPM_value * self.SiPM_label
                WeightedSiPM_row = self.SiPM_row * WeightedValue
                WeightedSiPM_col = self.SiPM_col * WeightedValue
                x = np.sum(WeightedSiPM_row) / np.sum(WeightedValue)
                y = np.sum(WeightedSiPM_col) / np.sum(WeightedValue)
            elif signal_type == 'C':
                Weightedlabel = self.SiPM_label - 1
                WeightedValue = self.SiPM_value * Weightedlabel
                WeightedSiPM_row = self.SiPM_row * WeightedValue
                WeightedSiPM_col = self.SiPM_col * WeightedValue
                x = np.sum(WeightedSiPM_row) / np.sum(WeightedValue)
                y = np.sum(WeightedSiPM_col) / np.sum(WeightedValue)

            self.bary_centre_col = col
            self.bary_centre_row = row
            return col,row
    
    
    def GetBarycentreRMS(self, signal_type = 'ALL'):
        if signal_type not in ['ALL', 'S', 'C']:
            warnings.warn(f"signal_type {signal_type} is not in 'ALL', 'S', 'C'...........Change to ALL ")
        Xbar, Ybar = self.GetBarycentre(signal_type)
        Xbar2 = Xbar*Xbar
        Ybar2 = Ybar*Ybar
        x = 0
        y = 0
        SiPM_X_square = self.SiPM_x * self.SiPM_x - Xbar2
        SiPM_Y_square = self.SiPM_y * self.SiPM_y - Ybar2
        if signal_type == 'ALL':
            Weighted_SiPM_X_square = SiPM_X_square * self.SiPM_value
            Weighted_SiPM_Y_square = SiPM_Y_square * self.SiPM_value
            x = np.sum(Weighted_SiPM_X_square) / np.sum(self.SiPM_value)
            y = np.sum(Weighted_SiPM_Y_square) / np.sum(self.SiPM_value)
        elif signal_type == 'S':
            WeightedValue = self.SiPM_value * self.SiPM_label
            Weighted_SiPM_X_square = SiPM_X_square * WeightedValue
            Weighted_SiPM_Y_square = SiPM_Y_square * WeightedValue
            x = np.sum(Weighted_SiPM_X_square) / np.sum(WeightedValue)
            y = np.sum(Weighted_SiPM_Y_square) / np.sum(WeightedValue)
        elif signal_type == 'C':
            Weightedlabel = self.SiPM_label - 1
            WeightedValue = self.SiPM_value * Weightedlabel
            Weighted_SiPM_X_square = SiPM_X_square * WeightedValue
            Weighted_SiPM_Y_square = SiPM_Y_square * WeightedValue
            x = np.sum(Weighted_SiPM_X_square) / np.sum(WeightedValue)
            y = np.sum(Weighted_SiPM_Y_square) / np.sum(WeightedValue)
        return np.sqrt(x), np.sqrt(y)
        
    
    def GetBarycentreRMSRowCol(self, signal_type = 'ALL'):
        if signal_type not in ['ALL', 'S', 'C']:
            warnings.warn(f"signal_type {signal_type} is not in 'ALL', 'S', 'C'...........Change to ALL ")
        col_bar, row_bar = self.GetBarycentreRowCol(signal_type)
        col_bar_2 = col_bar*col_bar
        row_bar_2 = row_bar*row_bar
        col = 0
        row = 0
        SiPM_Col_square = self.SiPM_col * self.SiPM_col - col_bar_2
        SiPM_Row_square = self.SiPM_row * self.SiPM_row - row_bar_2
        if signal_type == 'ALL':
            Weighted_SiPM_Col_square = SiPM_Col_square * self.SiPM_value
            Weighted_SiPM_Row_square = SiPM_Row_square * self.SiPM_value
            col = np.sum(Weighted_SiPM_Col_square) / np.sum(self.SiPM_value)
            row = np.sum(Weighted_SiPM_Row_square) / np.sum(self.SiPM_value)
        elif signal_type == 'S':
            WeightedValue = self.SiPM_value * self.SiPM_label
            Weighted_SiPM_Col_square = SiPM_Col_square * WeightedValue
            Weighted_SiPM_Row_square = SiPM_Row_square * WeightedValue
            col = np.sum(Weighted_SiPM_Col_square) / np.sum(WeightedValue)
            row = np.sum(Weighted_SiPM_Row_square) / np.sum(WeightedValue)
        elif signal_type == 'C':
            Weightedlabel = self.SiPM_label - 1
            WeightedValue = self.SiPM_value * self.SiPM_label
            Weighted_SiPM_Col_square = SiPM_Col_square * WeightedValue
            Weighted_SiPM_Row_square = SiPM_Row_square * WeightedValue
            col = np.sum(Weighted_SiPM_Col_square) / np.sum(WeightedValue)
            row = np.sum(Weighted_SiPM_Row_square) / np.sum(WeightedValue)
        return np.sqrt(col), np.sqrt(row)
    
    
    #def GetEnergyDensityX(self, max_radius = 8, signal_type = 'ALL'):
    #    if signal_type not in ['ALL', 'S', 'C']:
    #        warnings.warn(f"signal_type {signal_type} is not in 'ALL', 'S', 'C'...........Change to ALL ")
    #    Xbar, Ybar = self.GetBarycentre(signal_type)
    #    Xbar = round(Xbar)
    #    Ybar = round(Ybar)
    #
    #    energy_project_on_x = []
    #    energy_slice = []
    #    for j in range(NofFiberscolumn):
    #        energy_at_column_j = 0
    #        if signal_type == 'ALL':
    #            for i in range(NofFibersrow):
    #                if self.value_plot[i,j] <= 0 :
    #                    energy_at_column_j += 0
    #                else:
    #                    energy_at_column_j += self.value_plot[i,j]
    #        elif signal_type == 'S':
    #            for i in range(1,NofFibersrow,2):
    #                if self.value_plot[i,j] <= 0 :
    #                    energy_at_column_j += 0
    #                else:
    #                    energy_at_column_j += self.value_plot[i,j]
    #        elif signal_type == 'C':
    #            for i in range(0,NofFibersrow,2):
    #                if self.value_plot[i,j] <= 0 :
    #                    energy_at_column_j += 0
    #                else:
    #                    energy_at_column_j += self.value_plot[i,j]
    #        energy_project_on_x.append(energy_at_column_j)
    #    total_energy = sum(energy_project_on_x)
    #    
    #    for index in range(Xbar-max_radius, Xbar+max_radius+1):
    #        if index < 0:
    #            energy_slice.append(0)
    #        elif index > NofFiberscolumn - 1:
    #            energy_slice.append(0)
    #        else:
    #            energy_slice.append( energy_project_on_x[index]/total_energy )
    #    return energy_slice
    
    @classmethod
    def LoadReadoutValue(self, readout_value: list, signal_type: str):
        self.readout_value[signal_type] = readout_value
        self.signal_type = list(self.readout_value.keys())
    

    def SiPMGeoConverter(self, SiPMID: int, signal_type: str):
        """_summary_

        Args:
            SiPMID (int): _description_
            signal_type (string): _description_

        Returns:
            _type_: array position, physical position
        """
        # https://github.com/DRCalo/TBDataPreparation/blob/db59977b35ee10925991dd93b07e3ef8bee0fde9/2023_SPS/scripts/PhysicsEvent.h#L149
        # https://github.com/DRCalo/TBDataPreparation/blob/db59977b35ee10925991dd93b07e3ef8bee0fde9/2023_SPS/scripts/PhysicsEvent.h#L109
        colID = int( SiPMID%(NofFiberscolumn)      )
        rowID = int( 2*( SiPMID//(NofFiberscolumn) ) )
        SiPM_X, SiPM_Y = self.ConverterMix(colID, rowID)
        if signal_type == "S" : # even rows: row%2==0
            SiPM_Y = SiPM_Y - (sq3*tuberadius) 
        if signal_type == "C" : # uneven rows -> Add 1*dTubeY
            SiPM_X = SiPM_X + tuberadius
        return [colID,rowID],[SiPM_X, SiPM_Y]
    
    
    def ConverterMix(self, colID: int, rowID: int):
        SiPM_X = (-1) * ( moduleX/2 - tuberadius - (tuberadius*2)*colID )
        SiPM_Y = (-1*moduleY)/2 + tuberadius + (sq3*tuberadius)*( NofFibersrow - rowID ) + tuberadius*(2.*sq3m1-1.)
        return SiPM_X,SiPM_Y
    
    
    def SiPMGeoConverter_G4(self, SiPMID: int, signal_type: str):
        """  copynumber = ((NofFibersrow/2)*column+row/2);
            row%2==0:
                S_x = +moduleX/2 - tuberadius - (tuberadius*2+2*tolerance)*column;
                S_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance)*row+tuberadius*(2.*sq3m1-1.);
            row%2==1:
                C_x = moduleX/2 - tuberadius - tuberadius - (tuberadius*2+2*tolerance)*column;
                C_y = -moduleY/2 + tuberadius + (sq3*tuberadius+2*tolerance)*row+tuberadius*(2.*sq3m1-1.);

        Args:
            SiPMID (int): _description_
            signal_type (string): _description_

        Returns:
            _type_: array position, physical position
        """
        # https://github.com/DRCalo/TBDataPreparation/blob/db59977b35ee10925991dd93b07e3ef8bee0fde9/2023_SPS/scripts/PhysicsEvent.h#L149
        # https://github.com/DRCalo/TBDataPreparation/blob/db59977b35ee10925991dd93b07e3ef8bee0fde9/2023_SPS/scripts/PhysicsEvent.h#L109
        colID = int( SiPMID//(NofFibersrow/2)      )
        rowID = int( 2*( SiPMID%(NofFibersrow/2) ) )
        SiPM_X, SiPM_Y = self.ConverterMix_G4(colID, rowID)
        if signal_type == "C" : # uneven rows -> Add 1*dTubeY
            SiPM_X = SiPM_X + tuberadius
            SiPM_Y = SiPM_Y + (sq3*tuberadius)
        
        return [colID,rowID],[SiPM_X, SiPM_Y]

    
    def ConverterMix_G4(self, colID: int, rowID: int):
        SiPM_X = (-1) * ( moduleX/2 - tuberadius - (tuberadius*2)*colID )
        SiPM_Y = (-1*moduleY)/2 + tuberadius + (sq3*tuberadius)*rowID + tuberadius*(2.*sq3m1-1.)
        return SiPM_X,SiPM_Y
    
    
    def __str__(self):
        return '\n'.join(' '.join(str(x) for x in row) for row in self.value_plot)
    
    
    def draw(self, filter = 0, title=None, filename="test"):
        tmp_cmap = matcolors.Colormap(name = "test", N=1)
        fig, ax = plt.subplots(figsize=(8, 10), dpi=200)
        ax.scatter(self.SiPM_x, self.SiPM_y , c=self.SiPM_value, s=700, edgecolors='black', linewidths = 0.5, cmap='YlOrRd' )
        ax.set_xlabel("Horizontal [mm]")
        ax.set_ylabel("Vertical [mm]")
        for ind in range(len(self.SiPM_index)):
            if self.SiPM_label[ind] == 1:
                text = ax.text( self.SiPM_x[ind], self.SiPM_y[ind]-0.22, '%.2f' % self.SiPM_value[ind], ha="center", va="center", color="blue", size='x-small')
                text = ax.text( self.SiPM_x[ind], self.SiPM_y[ind]+0.23, 'S-'+str(int(self.SiPM_index[ind])) , ha="center", va="center", color="black", size='x-small')
            elif self.SiPM_label[ind] == 0:
                text = ax.text( self.SiPM_x[ind], self.SiPM_y[ind]-0.22, '%.2f' % self.SiPM_value[ind], ha="center", va="center", color="blue", size='x-small')
                text = ax.text( self.SiPM_x[ind], self.SiPM_y[ind]+0.23, 'C-'+str(int(self.SiPM_index[ind])) , ha="center", va="center", color="black", size='x-small')
        if not (title is None):
            ax.set_title(title)
        x = 0
        y = 0
        if self.bary_centre_x > -900 and self.bary_centre_y > -900 :
            x = self.bary_centre_x
            y = self.bary_centre_y
        else:
            x, y = self.GetBarycentre()
        ax.plot(x, y, 'go') 
        plt.savefig(filename+".png")
        plt.cla()
        plt.close(fig)
    
#a = SiPMDisplay(1,"S")
#print(a)