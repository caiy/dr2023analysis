import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array
import math

ColorPad = [6,7,8,9,1,2,3,4,5,12]

def drawTH1(input_data, xname, yname, title, nbins, xstart, xend, logY=False, outfile=""):
    canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
    if logY:
        canvas.SetLogy()
    if isinstance(input_data, dict) :
        hist_dict = {}
        legend = ROOT.TLegend(0.65,0.75,0.98,0.98)
        count = 0
        for histname, value in input_data.items():
            hist_dict[histname] = ROOT.TH1F(histname, title, nbins, xstart, xend)
            for i in value:
                hist_dict[histname].Fill(i)
            hist_dict[histname].SetLineColor(ColorPad[count])
            if count == 0:
                hist_dict[histname].GetXaxis().SetTitle(xname)
                hist_dict[histname].GetYaxis().SetTitle(yname)
                hist_dict[histname].Draw("LE")
            else:
                hist_dict[histname].Draw("ELSAME")
            legend.AddEntry(hist_dict[histname] , histname,"le")
            count += 1
        legend.Draw()
    else:
        hist = ROOT.TH1F(title, title, nbins, xstart, xend)
        for i in input_data:
            hist.Fill(i)
        hist.GetXaxis().SetTitle(xname)
        hist.GetYaxis().SetTitle(yname)
        hist.Draw()
    if outfile == "":
        outname = str(title+"_"+xname+"_"+yname)
    else:
        outname = outfile
    canvas.SaveAs(str(outname+".pdf"))

def drawTH1Average(input_data, weight, xname, yname, title, nbins, xstart, ystart, outfile=""):
    canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
    hist_weight = ROOT.TH1F(str(title+"_weighted"), title, nbins, xstart, ystart)
    hist_one    = ROOT.TH1F(str(title+"_noweight"), title, nbins, xstart, ystart)
    for i in range(len(input_data)):
        hist_one.Fill( input_data[i] )
        hist_weight.Fill(  input_data[i], weight[i] )
    hist_weight.GetXaxis().SetTitle(xname)
    hist_weight.GetYaxis().SetTitle(yname)
    hist_weight.Divide(hist_one)
    hist_weight.Draw()
    if outfile == "":
        outname = str(title+"_"+xname+"_"+yname)
    else:
        outname = outfile
    canvas.SaveAs(str(outname+".pdf"))

def drawTH2(input_data_x, input_data_y, xname, yname, title, nbinsx, xstart, xend, nbinsy, ystart, yend, outfile=""):
    canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
    hist_weight = ROOT.TH2F(title, title, nbinsx, xstart, xend, nbinsy, ystart, yend)
    for a,b in zip( input_data_x, input_data_y ):
        hist_weight.Fill(a,b)
    hist_weight.GetXaxis().SetTitle(xname)
    hist_weight.GetYaxis().SetTitle(yname)
    hist_weight.Draw("COLZ")
    if outfile == "":
        outname = str(title+"_"+xname+"_"+yname)
    else:
        outname = outfile
    canvas.SaveAs(str(outname+".pdf"))

def draw2DScatter(input_data_x, input_data_y, xname, yname, title, size = 0.3 , outfile=""):
    x = [-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0 ]
    y = [-1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
    
    # Create a TGraph object
    graph = ROOT.TGraph(len(input_data_x), array('f', input_data_x), array('f', input_data_y))

    # Set the title and axis labels
    graph.SetTitle(title.split('/')[-1])
    graph.GetXaxis().SetTitle(xname)
    graph.GetYaxis().SetTitle(yname)
    graph.SetMarkerStyle(8)
    graph.SetMarkerSize(size)

    # Create a canvas to draw the scatter plot
    canvas = ROOT.TCanvas(title, title, 800, 600)

    # Draw the scatter plot
    graph.Draw("AP")

    #graph.GetXaxis().SetRangeUser(0.2,0.4)
    #graph.GetYaxis().SetRangeUser(0.2,0.4)
    
    # Show the canvas
    if outfile == "":
        outname = str(title+"_"+xname+"_"+yname)
    else:
        outname = outfile
    canvas.SaveAs(str(outname+".pdf"))
    canvas.Draw()


def draw2DScatterColored(input_data_x, input_data_y, input_color, xname, yname, title, size = 3 , outfile=""):
    fig, ax = plt.subplots()
    sc = ax.scatter(input_data_x, input_data_y, c=input_color, s=size, edgecolors='none')

    ax.legend()
    ax.grid(True)
    
    ax.set_xlabel(xname)
    ax.set_ylabel(yname)
    fig.suptitle(title)
    plt.colorbar(sc)
    # Show the canvas
    if outfile == "":
        outname = str(title+"_"+xname+"_"+yname)
    else:
        outname = outfile
    plt.savefig(outname+".pdf")
