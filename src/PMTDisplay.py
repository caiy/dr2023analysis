import uproot
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array
import math
import warnings


# Copied from @ANDREA 
sq3=1.733
sq3m1=sq3/3.
d1 = 1740.     # distance in mm from DWC1 to DWC2
d2 = 2475.     # distance in mm from DWC2 to calorimeter
dPS = 175.     # distance in mm from calorimeter to PreShower
NofFibersrow = 20   # number of rows in one module
NofFiberscolumn = 16     # number of fiber columns in one module
tuberadius = 1.0
dtubeY=sq3*tuberadius                                       # dtubeY given in simulation before rotation
dtubeX=2.*tuberadius
moduleX = (2*NofFiberscolumn+1)*tuberadius
moduleY = (NofFibersrow-1.)*dtubeY+4.*tuberadius*sq3m1


class PMTDisplay:
    def __init__(self, readout_value: list, SiPM: int = 0 ):
        self.value_plot = np.zeros((3,3))
        self.index_plot = np.zeros((3,3))
        self.readout_value = readout_value
        self.SiPM = SiPM
        self.RunPMTGeoConverter()

    def RunPMTGeoConverter(self ):
        for i in range(len(self.readout_value)):
            cord = self.PMTGeoConverter(i)
            self.value_plot[cord[0]][cord[1]] = self.readout_value[i]
            self.index_plot[cord[0]][cord[1]] = i+1
        self.value_plot[1][1] = self.SiPM
        self.index_plot[1][1] = 0
    
    def PMTGeoConverter(self, index: int):
        if index <= 2:
            x=2
            y=index
        elif index >=5:
            x=0
            y=index-5
        elif index == 3:
            x=1
            y=0
        elif index == 4:
            x=1
            y=2
        return [x,y]
    
    
    def __str__(self):
        return '\n'.join(' '.join(str(x) for x in row) for row in self.value_plot)
    
    
    def draw(self, filter = 0, title=None, filename="test"):
        fig, ax = plt.subplots(figsize=(5, 5))
        im = ax.imshow(self.value_plot)
        for i in range(3):
            for j in range(3):
                text = ax.text(j-0.2, i-0.2, '%.2f' % self.value_plot[i,j], ha="center", va="center", color="black", size="x-large")
                if self.index_plot[i,j] != 0:
                    text = ax.text(j+0.2, i+0.2, 'PMT-%.0f' % self.index_plot[i,j] , ha="center", va="center", color="white", size="x-large")
                elif self.index_plot[i,j] == 0:
                    text = ax.text(j+0.2, i+0.2, 'SiPM-%.0f' % self.index_plot[i,j] , ha="center", va="center", color="white", size="x-large")
        if not (title is None):
            ax.set_title(title)
        plt.savefig(filename+".png")
        plt.cla()
        plt.close(fig)
    
#a = SiPMDisplay(1,"S")
#print(a)