import uproot
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array
import math
import warnings


# Copied from @ANDREA 
sq3=1.733
sq3m1=sq3/3.
d1 = 1740.     # distance in mm from DWC1 to DWC2
d2 = 2475.     # distance in mm from DWC2 to calorimeter
dPS = 175.     # distance in mm from calorimeter to PreShower
NofFibersrow = 20   # number of rows in one module
NofFiberscolumn = 16     # number of fiber columns in one module
tuberadius = 1.0
dtubeY=sq3*tuberadius                                       # dtubeY given in simulation before rotation
dtubeX=2.*tuberadius
moduleX = (2*NofFiberscolumn+1)*tuberadius
moduleY = (NofFibersrow-1.)*dtubeY+4.*tuberadius*sq3m1


class DWCDisplay:
    def __init__(self, DWC1_x, DWC1_y, DWC2_x = 0 , DWC2_y = 0, correction_x = 0, correction_y = 0 ):
        self.DWC1_x = DWC1_x
        self.DWC1_y = DWC1_y
        self.DWC2_x = DWC2_x
        self.DWC2_y = DWC2_y
        self.correction_x = correction_x
        self.correction_y = correction_y
    
    def GetPredictCenter(self):
        DeltaXDWC = self.DWC1_x - self.DWC2_x - self.correction_x 
        DeltaYDWC = self.DWC1_y - self.DWC2_y - self.correction_y 
        
        ProjectX = self.DWC1_x + DeltaXDWC * (d2+d1)/d1
        ProjectY = self.DWC1_y + DeltaYDWC * (d2+d1)/d1
        
        return ProjectX,ProjectY
    
    