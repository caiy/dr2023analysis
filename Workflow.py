import uproot
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from array import array
import math
import SiPMDisplay
import PMTDisplay
import random
import DWCDisplay

ROOT.gROOT.SetBatch(ROOT.kTRUE)
ColorPad = [6,7,8,9,1,2,3,4,5,12]

class Workflow2023:

    def drawTH1(input_data, xname, yname, title, nbins, xstart, xend, logY=False, outfile=""):
        canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
        if logY:
            canvas.SetLogy()
        if isinstance(input_data, dict) :
            hist_dict = {}
            legend = ROOT.TLegend(0.65,0.75,0.98,0.98)
            count = 0
            for histname, value in input_data.items():
                hist_dict[histname] = ROOT.TH1F(histname, title, nbins, xstart, xend)
                for i in value:
                    hist_dict[histname].Fill(i)
                hist_dict[histname].SetLineColor(ColorPad[count])
                if count == 0:
                    hist_dict[histname].GetXaxis().SetTitle(xname)
                    hist_dict[histname].GetYaxis().SetTitle(yname)
                    hist_dict[histname].Draw("LE")
                else:
                    hist_dict[histname].Draw("ELSAME")
                legend.AddEntry(hist_dict[histname] , histname,"le")
                count += 1
            legend.Draw()
        else:
            hist = ROOT.TH1F(title, title, nbins, xstart, xend)
            for i in input_data:
                hist.Fill(i)
            hist.GetXaxis().SetTitle(xname)
            hist.GetYaxis().SetTitle(yname)
            hist.Draw()
        if outfile == "":
            outname = str(title+"_"+xname+"_"+yname)
        else:
            outname = outfile
        canvas.SaveAs(str(outname+".pdf"))

    def drawTH1Average(input_data, weight, xname, yname, title, nbins, xstart, ystart, outfile=""):
        canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
        hist_weight = ROOT.TH1F(str(title+"_weighted"), title, nbins, xstart, ystart)
        hist_one    = ROOT.TH1F(str(title+"_noweight"), title, nbins, xstart, ystart)
        for i in range(len(input_data)):
            hist_one.Fill( input_data[i] )
            hist_weight.Fill(  input_data[i], weight[i] )
        hist_weight.GetXaxis().SetTitle(xname)
        hist_weight.GetYaxis().SetTitle(yname)
        hist_weight.Divide(hist_one)
        hist_weight.Draw()
        if outfile == "":
            outname = str(title+"_"+xname+"_"+yname)
        else:
            outname = outfile
        canvas.SaveAs(str(outname+".pdf"))

    def drawTH2(input_data_x, input_data_y, xname, yname, title, nbinsx, xstart, xend, nbinsy, ystart, yend, outfile=""):
        canvas = ROOT.TCanvas(title, title, 0, 0, 800, 600)
        hist_weight = ROOT.TH2F(title, title, nbinsx, xstart, xend, nbinsy, ystart, yend)
        for a,b in zip( input_data_x, input_data_y ):
            hist_weight.Fill(a,b)
        hist_weight.GetXaxis().SetTitle(xname)
        hist_weight.GetYaxis().SetTitle(yname)
        hist_weight.Draw("COLZ")
        if outfile == "":
            outname = str(title+"_"+xname+"_"+yname)
        else:
            outname = outfile
        canvas.SaveAs(str(outname+".pdf"))

    def draw2DScatter(input_data_x, input_data_y, xname, yname, title, size = 0.3 , outfile=""):
        x = [-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0 ]
        y = [-1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]

        # Create a TGraph object
        graph = ROOT.TGraph(len(input_data_x), array('f', input_data_x), array('f', input_data_y))

        # Set the title and axis labels
        graph.SetTitle(title.split('/')[-1])
        graph.GetXaxis().SetTitle(xname)
        graph.GetYaxis().SetTitle(yname)
        graph.SetMarkerStyle(8)
        graph.SetMarkerSize(size)

        # Create a canvas to draw the scatter plot
        canvas = ROOT.TCanvas(title, title, 800, 600)

        # Draw the scatter plot
        graph.Draw("AP")

        # Show the canvas
        if outfile == "":
            outname = str(title+"_"+xname+"_"+yname)
        else:
            outname = outfile
        canvas.SaveAs(str(outname+".pdf"))
        canvas.Draw()


    def draw2DScatterColored(input_data_x, input_data_y, input_color, xname, yname, title, size = 3 , outfile=""):
        fig, ax = plt.subplots()
        sc = ax.scatter(input_data_x, input_data_y, c=input_color, s=size, edgecolors='none')

        ax.legend()
        ax.grid(True)

        ax.set_xlabel(xname)
        ax.set_ylabel(yname)
        fig.suptitle(title)
        plt.colorbar(sc)
        # Show the canvas
        if outfile == "":
            outname = str(title+"_"+xname+"_"+yname)
        else:
            outname = outfile
        plt.savefig(outname+".pdf")



    def correctDWCs( pds ):
        pds["DeltaXDWC"] = (pds['XDWC1'] - pds['XDWC2'])
        pds["DeltaYDWC"] = (pds['YDWC1'] - pds['YDWC2'])

        correction_x = pds["DeltaXDWC"].sum()/len(pds.index)
        correction_y = pds["DeltaYDWC"].sum()/len(pds.index)    
        pds["DeltaXDWC"] = (pds['XDWC1'] - pds['XDWC2'] - correction_x )
        pds["DeltaYDWC"] = (pds['YDWC1'] - pds['YDWC2'] - correction_y )

        pds["DDistance"] = np.sqrt(pds["DeltaXDWC"]*pds["DeltaXDWC"] + pds["DeltaYDWC"]*pds["DeltaYDWC"])

        return correction_x, correction_y


    def registerPlotList(self, pds, title_prefix, outfile_prefix ):
        self.draw2DScatterColored(pds["XDWC1"], pds["XDWC2"], pds["DeltaYDWC"], xname="XDWC1", yname="XDWC2", title=title_prefix+" DWCx", outfile=outfile_prefix+"_1test" )
        self.draw2DScatterColored(pds["YDWC1"], pds["YDWC2"], pds["DeltaXDWC"], xname="YDWC1", yname="YDWC2", title=title_prefix+" DWCy", outfile=outfile_prefix+"_2test" )
        self.drawTH1(pds["MCounter"], xname="MCounter", yname="Event", title=title_prefix+ " MCounter", nbins=100, xstart=0, xend=1000, logY=True, outfile=outfile_prefix+"_MCounter" )
        self.drawTH1(pds["PShower"], xname="PShower", yname="Event", title=title_prefix+" PShower", nbins=200, xstart=0, xend=2000, logY=True, outfile=outfile_prefix+"_PShower" )
        self.drawTH1(pds["C1"], xname="C1", yname="Event", title=title_prefix+" C1", nbins=100, xstart=0, xend=1000, outfile=outfile_prefix+"_C1" )
        self.drawTH1(pds["C2"], xname="C2", yname="Event", title=title_prefix+" C2", nbins=100, xstart=0, xend=1000, outfile=outfile_prefix+"_C2" )
        self.draw2DScatter(pds["DeltaXDWC"], pds["DeltaYDWC"], xname="DeltaXDWC", yname="DeltaYDWC", title=title_prefix+" DeltaDWCs", outfile=outfile_prefix+"_DeltaDWCs" )
        self.drawTH2(pds["DeltaXDWC"], pds["DeltaYDWC"], xname="DeltaXDWC", yname="DeltaYDWC", title=title_prefix+" DeltaDWCs", 
                nbinsx=100, xstart=-50, xend=50, nbinsy=100, ystart=-50, yend=50, outfile=outfile_prefix+"_DeltaDWCs_TH2")
        self.drawTH1(pds["DeltaXDWC"], xname="DeltaXDWC", yname="Event", title=title_prefix+" DeltaXDWC", nbins=100, xstart=-50, xend=50, outfile=outfile_prefix+"_DeltaXDWC" )
        self.drawTH1(pds["DeltaYDWC"], xname="DeltaYDWC", yname="Event", title=title_prefix+" DeltaYDWC", nbins=100, xstart=-50, xend=50, outfile=outfile_prefix+"_DeltaYDWC" )
        self.draw2DScatter(pds["C1"], pds["C2"], xname="C1", yname="C2", title=title_prefix+" C1 vs C2", outfile=outfile_prefix+"_C1vsC2" )
        self.drawTH1(pds["totSiPMSene"], xname="SiPM S Energy", yname="Event", title=title_prefix+" totSiPMSene", nbins=100, xstart=0, xend=25, logY=True, outfile=outfile_prefix+"_totSiPMSene" )
        self.drawTH1(pds["totSiPMCene"], xname="SiPM C Energy", yname="Event", title=title_prefix+" totSiPMCene", nbins=100, xstart=0, xend=25, logY=True, outfile=outfile_prefix+"_totSiPMCene" )
        self.draw2DScatter(pds["XDWC1"], pds["YDWC1"], xname="XDWC1", yname="YDWC1", title=title_prefix+" DWC1", outfile=outfile_prefix+"_DWC1" )
        self.draw2DScatter(pds["XDWC2"], pds["YDWC2"], xname="XDWC2", yname="YDWC2", title=title_prefix+" DWC2", outfile=outfile_prefix+"_DWC2" )
        self.drawTH2(pds["XDWC1"], pds["YDWC1"], xname="XDWC1", yname="YDWC1", title=title_prefix+" DWC1", 
                nbinsx=100, xstart=-50, xend=50, nbinsy=100, ystart=-50, yend=50, outfile=outfile_prefix+"_DWC1_TH2")
        self.drawTH2(pds["XDWC2"], pds["YDWC2"], xname="XDWC2", yname="YDWC2", title=title_prefix+" DWC2", 
                nbinsx=100, xstart=-50, xend=50, nbinsy=100, ystart=-50, yend=50, outfile=outfile_prefix+"_DWC2_TH2")


    def registerCut(self, cut_string, draw_plot = True ):
        return 0

    def runOneFile(self, input_root_file, output_prefix):
        runnumber = input_root_file[-11:-5]
        # Open the root file
        root_file = uproot.open(input_root_file)

        # Get the keys (top-level objects) in the root file
        root_tree = root_file["Ftree/Events"]

        pds = root_tree.array(library="pd")

        self.correctDWCs(pds)

        print(" ***** X ***** ", pds["DeltaXDWC"].sum()/len(pds.index))
        print(" ***** Y ***** ", pds["DeltaYDWC"].sum()/len(pds.index))
        # No cut

        print("start: ", len(pds))
        self.registerPlotList(pds, runnumber, output_prefix+runnumber+"_0" )
        #
        pds=pds[ (pds["MCounter"] > 160) ]
        print("MCounter > 160: ", len(pds))
        self.registerPlotList(pds, runnumber, output_prefix+runnumber+"_1" )
        #
        #pds=pds[ (pds["PShower"] > 380) ]
        #print("PShower > 380: ", len(pds))
        #registerPlotList(pds, runnumber, output_prefix+runnumber+"_2" )
        #
        pds=pds[ (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) ]
        print("DWC < 2: ", len(pds))
        self.registerPlotList(pds, runnumber, output_prefix+runnumber+"_3" )
        self.drawTH2(pds["DeltaXDWC"], pds["DeltaYDWC"], xname="DeltaXDWC", yname="DeltaYDWC", title=runnumber+" DeltaDWCs", 
                nbinsx=30, xstart=-3, xend=3, nbinsy=30, ystart=-3, yend=3, outfile=output_prefix+runnumber+"_3"+"_DeltaDWCs_TH2")

        SiPMEnergy = pds["totSiPMSene"].sum() + pds["totSiPMCene"].sum()
        PMTEnergy = pds["SPMTenergy"].sum() + pds["CPMTenergy"].sum()

        SiPMEnergy /= len(pds.index)
        PMTEnergy /= len(pds.index)

        #print(SiPMEnergy, PMTEnergy)
        return SiPMEnergy,PMTEnergy


    def runSiPM(self, input_root_file, output_prefix, selectPreShower = False ):
        runnumber = input_root_file[-11:-5]
        # Open the root file
        root_file = uproot.open(input_root_file)
        # Get the keys (top-level objects) in the root file
        root_tree = root_file["Ftree/Events"]

        pds = root_tree.array(library="pd")

        correct_x, correct_y = correctDWCs(pds)

        pds=pds[ (pds["MCounter"] > 160) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] + pds["totSiPMCene"] > 0.1) ] # & (pds["PShower"] > 380)
        #registerPlotList(pds, runnumber, output_prefix+runnumber )

        draw10_Active_Board = 0
        for event_index, branch_values in pds.iterrows():
            #if branch_values["MCounter"] > 200 and branch_values["totSiPMSene"] > 0.1 :
            if True:

                tmp_DWCs = DWCDisplay.DWCDisplay( DWC1_x = branch_values["XDWC1"] , DWC1_y = branch_values["YDWC1"], DWC2_x = branch_values["XDWC2"] , DWC2_y = branch_values["YDWC2"], correction_x = correct_x, correction_y = correct_y )
                pds.loc[event_index,'predictX'], pds.loc[event_index,'predictY'] = tmp_DWCs.GetPredictCenter()

                tmp_list_S = []
                tmp_list_C = []

                for i in range(160):
                    tmp_list_S.append( branch_values["SiPMPheS[160]["+str(i)+"]"] )
                    tmp_list_C.append( branch_values["SiPMPheC[160]["+str(i)+"]"] )
                tmp_SiPMS =  SiPMDisplay.SiPMDisplay( {"S":tmp_list_S, "C":tmp_list_C} )
                pds.loc[event_index,'NumberOfActiveBoard'] = tmp_SiPMS.NumberOfActiveBoard()

                pds.loc[event_index,'barycentreX'], pds.loc[event_index,'barycentreY'] = tmp_SiPMS.GetBarycentre()

                pds.loc[event_index,'barycentreWidthX'], pds.loc[event_index,'barycentreWidthY'] = tmp_SiPMS.GetBarycentreRMS()

                #if tmp_SiPMS.NumberOfActiveBoard() > 0:
                #    if tmp_SiPMS.NumberOfActiveBoard() == 1 and draw10_Active_Board < 5 :
                #        tmp_SiPMS.draw(title="ActiveBoard1 "+runnumber+" Event"+str(branch_values["EventID"]), filename=output_prefix+runnumber+"_Event"+str(branch_values["EventID"]) )
                #        draw10_Active_Board += 1
                #        print(tmp_SiPMS.NumberOfActiveBoard())
                #        print( tmp_SiPMS.PeakPosition() )
                #        print( tmp_SiPMS.PeakPosition(2) )
                #        most_active_board = []
                #        for i in tmp_SiPMS.getActiveBoard():
                #            if ( sum(i[1]) > sum(i[3]) ):
                #                most_active_board = i[1]
                #            else :
                #                most_active_board = i[3]
                #        draw2DScatter(most_active_board, np.arange(1,17,1), xname="signal strength", yname="position", 
                #                     size = 0.8, title=output_prefix+"MostActiveLine "+runnumber+" Event"+str(branch_values["EventID"]))
                #    if tmp_SiPMS.NumberOfActiveBoard() > 4:
                #        print(tmp_SiPMS.NumberOfActiveBoard())
                #        tmp_SiPMS.draw(title="ActiveBoard"+str(tmp_SiPMS.NumberOfActiveBoard())+" "+runnumber+" Event"+str(branch_values["EventID"]), filename=output_prefix+runnumber+"_Event"+str(branch_values["EventID"]) )
                #        print( tmp_SiPMS.PeakPosition() )
                #        print( tmp_SiPMS.PeakPosition(2) )
                #continue
        #drawTH1(pds['NumberOfActiveBoard'], xname="Number of Active Board", yname="Event", title=output_prefix+runnumber, nbins=5, xstart=0, xend=5, logY=True)

        self.draw2DScatter(pds["predictX"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX" )
        self.draw2DScatter(pds["predictY"], pds["barycentreY"], xname="Predict CaloY [mm]", yname="Barycenter CaloY [row number]", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsCaloY" )
        self.draw2DScatterColored(pds["predictX"], pds["barycentreX"], pds["DeltaXDWC"], xname="Predict CaloX [mm]", yname="Barycenter CaloX [column number]", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsCaloX_colored" )
        self.draw2DScatterColored(pds["predictX"], pds["PShower"], pds["barycentreX"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X_colored" )

        self.draw2DScatter(pds["predictX"], pds["PShower"], xname="Predict CaloX [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWCvsPS_X" )
        self.draw2DScatter(pds["predictY"], pds["PShower"], xname="Predict CaloY [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWCvsPS_Y" )

        self.draw2DScatter(pds["XDWC1"], pds["PShower"], xname="DWC1 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC1vsPS_X" )
        self.draw2DScatter(pds["YDWC1"], pds["PShower"], xname="DWC1 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC1vsPS_Y" )

        self.draw2DScatter(pds["XDWC2"], pds["PShower"], xname="DWC2 X [mm]", yname="PreShower", title=runnumber+" X", outfile=output_prefix+runnumber+"_DWC2vsPS_X" )
        self.draw2DScatter(pds["YDWC2"], pds["PShower"], xname="DWC2 Y [mm]", yname="PreShower", title=runnumber+" Y", outfile=output_prefix+runnumber+"_DWC2vsPS_Y" )

        self.drawTH2(pds["predictX"], pds["predictY"], xname="Predict CaloX [mm]", yname="Predict CaloY [mm]", title=runnumber+" DWC prediction", 
                nbinsx=40, xstart=-20, xend=20, nbinsy=40, ystart=-20, yend=20, outfile=output_prefix+runnumber+"_DWCXvsDWCY")
        self.drawTH2(pds["barycentreX"], pds["barycentreY"], xname="Barycenter CaloX [column number]", yname="Barycenter CaloY [row number]", title=runnumber+" Barycenter", 
                nbinsx=20, xstart=0, xend=20, nbinsy=20, ystart=0, yend=20, outfile=output_prefix+runnumber+"_CaloXvsCaloY")
        #pds=pds[ (pds["NumberOfActiveBoard"] > 4) ]
        #registerPlotList(pds, runnumber, output_prefix+runnumber+"_5board" )
        return pds['barycentreWidthX'].mean(), pds['barycentreWidthY'].mean()
        if selectPreShower :
            pds=pds[ (pds["predictY"] > 8) & (abs(pds["predictY"]) < 14) & (abs(pds["predictX"]) > 4) & ( pds["predictX"] < 8) ]
            registerPlotList(pds, runnumber, output_prefix+runnumber+"_CenterCut" )



    def runPMT(input_root_file, output_prefix, selectPreShower = False ):
        runnumber = input_root_file[-11:-5]
        # Open the root file
        root_file = uproot.open(input_root_file)
        # Get the keys (top-level objects) in the root file
        root_tree = root_file["Ftree/Events"]

        pds = root_tree.array(library="pd")

        correct_x, correct_y = correctDWCs(pds)

        pds=pds[ (pds["MCounter"] > 160) & (abs(pds["DeltaXDWC"]) < 2) & (abs(pds["DeltaYDWC"]) < 2) & ( pds["totSiPMSene"] + pds["totSiPMCene"] > 0.1) ] # & (pds["PShower"] > 380)

        pds['totPMT1'] = pds['SPMT1'] + pds['CPMT1']
        pds['totPMT2'] = pds['SPMT2'] + pds['CPMT2']
        pds['totPMT3'] = pds['SPMT3'] + pds['CPMT3']
        pds['totPMT4'] = pds['SPMT4'] + pds['CPMT4']
        pds['totPMT5'] = pds['SPMT5'] + pds['CPMT5']
        pds['totPMT6'] = pds['SPMT6'] + pds['CPMT6']
        pds['totPMT7'] = pds['SPMT7'] + pds['CPMT7']
        pds['totPMT8'] = pds['SPMT8'] + pds['CPMT8']
        #registerPlotList(pds, runnumber, output_prefix+runnumber )
        pmt_dict = {}
        pmt_dict['1'] = pds['totPMT1'].mean()
        pmt_dict['2'] = pds['totPMT2'].mean()
        pmt_dict['3'] = pds['totPMT3'].mean()
        pmt_dict['4'] = pds['totPMT4'].mean()
        pmt_dict['5'] = pds['totPMT5'].mean()
        pmt_dict['6'] = pds['totPMT6'].mean()
        pmt_dict['7'] = pds['totPMT7'].mean()
        pmt_dict['8'] = pds['totPMT8'].mean()

        return pmt_dict